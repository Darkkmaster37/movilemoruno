import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ServiceService } from '../services/service.service';
import { LoadingController, ModalController, ToastController, AlertController } from '@ionic/angular';
import { SearchPagePage } from '../search-page/search-page.page';
import * as moment from 'moment';
import { HorasPage } from '../horas/horas.page';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.page.html',
  styleUrls: ['./create-page.page.scss'],
})
export class CreatePagePage implements OnInit {
  @Input() tipo: string;
  form: FormGroup;
  pacientes: any;
  control: any;
  apellidos: any;
  nombres: any;
  actual: string;
  actualHora: string;
  tratamientos: any;
  tratamiento:any;
  tratamiento_emergencia: any;
  dataHour: any;
  id: any;
  hora = [];
  horas = '00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23'
  arr: string[];
  horasTime = ['10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00',
    '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00',
    '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00',
    '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00'];
  controlHora: any;
  fechaLoad: any;
  conditionNombreNuevo = false;
  conditionHora = false;
  observacion:any;
  hourRecover: any;
  constructor(
    public formB: FormBuilder,
    private service: ServiceService,
    private modalController: ModalController,
    public loading: LoadingController,
    public toast: ToastController,
    public modal: ModalController,
    public alertController:AlertController
  ) {
  }

  ngOnInit() {
    if (this.tipo == 'reserva') {
      this.form = this.formB.group({
        fecha: ['', Validators.required],
        // hora: ['', Validators.required],
        tipo: ['', Validators.required],
        // numFicha: ['', Validators.required],
        // tratamiento: ['', Validators.required]
      });
    } else if (this.tipo == 'emergencia') {
      this.form = this.formB.group({
        fecha: ['', Validators.required],
        hora: ['', Validators.required],
        // tipo: ['', Validators.required],
      });
    }
    this.getTratamientos();
    // this.getHora();
    this.control = false;
    this.actual = moment().format('YYYY-MM-DD');
    this.actualHora = moment().format('HH:mm');
  }

  async searchCliente(): Promise<void> {
    this.conditionNombreNuevo = false;
    const modal = await this.modalController.create({
      component: SearchPagePage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
    });
    await modal.present();

    const { data } = await modal.onDidDismiss();
    this.control = data.control;
    this.nombres = data.nombres;
    this.apellidos = data.apellidos;
    this.id = data.id;
  }

  async searchHora(): Promise<void> {
    this.hourRecover = this.fechaLoad;
    // if (this.fechaLoad) {
      const modal = await this.modalController.create({
        component: HorasPage,
        cssClass: 'my-custom-class',
        swipeToClose: true,
        componentProps: {
          'fecha': this.hourRecover
        }
      });
      await modal.present();
  
      const { data } = await modal.onDidDismiss();
      this.controlHora = data.controlHora;
      // this.nombres = data.nombres;
      // this.apellidos = data.apellidos;
      this.dataHour = data.data;
      this.fechaLoad = data.dateFechaReload; 
      console.log(this.fechaLoad);
           
    // } else {
      // this.presentAlertConfirm();

    // }
    
  }
  
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Alerta!',
      message: 'Se equivoco de hora tiene que seleccionar nuevamente la fecha!!!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.form.reset()
          }
        }
      ]
    });
  
    await alert.present();
  }

  eventDate(e){
    this.fechaLoad = moment(e.detail.value).format('YYYY-MM-DD');
    this.conditionHora = true;
  }

  getTratamientos(): void {
    this.service.getTratamientos().subscribe((data) => {
      this.tratamientos = data.data;
    });
  }

  async createReserva(): Promise<void> {    
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    await loading.present().then(async () => {
      if (this.tipo == 'reserva') {
        if (this.id == '') {          
          this.id = null;
        }
        this.service.createReserva(this.id, moment(this.form.value.fecha).format('YYYY-MM-DD'),
          this.dataHour, this.form.value.tipo, this.form, this.observacion, this.tratamiento).subscribe((data) => {
            this.ok('Reserva creada');
            loading.dismiss();
            this.dismiss();
          }, err => {
            loading.dismiss();
            this.error('No se registro la reserva');
          })
      } else if (this.tipo == 'emergencia') {
        this.service.createEmergencia(this.id, moment(this.form.value.fecha).format('YYYY-MM-DD'),
          moment(this.form.value.hora).format('HH:mm'), this.observacion,this.tratamiento_emergencia).subscribe((data) => {
            this.ok('Emergencia creada');
            loading.dismiss();
            this.dismiss();
          }, err => {
            loading.dismiss();
            this.error('No se registro la emergencia');
          })
      }
    });
  }

  dismiss(): void {
    this.modal.dismiss();
  }

  async ok(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async error(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  getHora() {
    this.service.searchReserva(this.dataHour).subscribe((data) => {
      for (let index = 0; index < data.data.length; index++) {
        let hor = data.data[index].hora;
        this.hora.push(hor);
      }
      for (let index = 0; index < this.hora.length; index++) {
        this.horasTime.splice(this.horasTime.indexOf(this.hora[index]), 1)
      }      
    });
  }

  openText(){
    this.id = null;
    this.control = false;
    this.conditionNombreNuevo = true;
  }

}
