import { Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, AnimationController, LoadingController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';
import { ViewModalPage } from '../view-modal/view-modal.page';
import { EditModalPage } from '../edit-modal/edit-modal.page';
import { Router } from '@angular/router';
import { CreatePagePage } from '../create-page/create-page.page';
import * as moment from 'moment';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild('loadingIcon', { read: ElementRef }) loadingIcon: ElementRef;
  reservas: [];
  det: string;
  fecha:any;
  buttonOn: boolean;
  actual: string;

  // type:any;
  constructor(
    private service: ServiceService,
    private animationCtrl: AnimationController,
    public loading: LoadingController,
    private modalController: ModalController,
    private alert: AlertController,
    public toast: ToastController,
    public router: Router
  ) {
    this.actual = moment().format('YYYY-MM-DD');
    this.getReservas();
  }

  ionViewDidEnter() {
    this.getReservas();
    this.fecha = '';

  }

  async getReservas(): Promise<void> {
    this.fecha = '';
    this.buttonOn = true;
    this.det = '';
    this.reservas = [];
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    this.animate();
    await loading.present().then(async () => {
      this.service.getReservasDia().subscribe(async (data) => {
        this.reservas = data.data;
        this.det = 'lleno';
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });
    });

  }

  async searchFilterReserva(): Promise<void> {
    let fechaFilter = moment(this.fecha).format('YYYY-MM-DD');
    this.det = '';
    this.reservas = [];
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    this.animate();
    await loading.present().then(async () => {
      this.service.searchReserva(fechaFilter).subscribe(async (data) => {
        this.reservas = data.data;
        this.det = 'lleno';
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });
    });

  }

  reset(){
    this.buttonOn = false;
    this.fecha = '';
    this.getReservas();
  }

  animate(): void {
    const loadingAnimation = this.animationCtrl.create('loading-animation')
      .addElement(this.loadingIcon.nativeElement)
      .duration(1500)
      .iterations(3)
      .fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
    loadingAnimation.play();
  }

  async viewReserva(id,item): Promise<void> {
    const modal = await this.modalController.create({
      component: ViewModalPage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        'id': id,
        'tipo': 'reserva',
        'item':item
      }
    });
    return await modal.present();
  }

  async editReserva(item): Promise<void> {
    const modal = await this.modalController.create({
      component: EditModalPage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        'item': item,
        'tipo': 'reserva'
      }
    });
    modal.onDidDismiss().then(() => {
      this.getReservas();
    });
    return await modal.present();
  }

  async deleteReserva(id): Promise<void> {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: '¿Eliminar Reserva?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.service.deleteReserva(id).subscribe((data) => {
              this.ok('Reserva eliminada');
              this.getReservas();
            }, err => {
              this.error(err);
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async createReserva(): Promise<void> {
    const modal = await this.modalController.create({
      component: CreatePagePage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        'tipo': 'reserva'
      }
    });
    modal.onDidDismiss().then(() => {
      this.getReservas();
    });
    return await modal.present();
  }

  async ok(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async error(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

}
