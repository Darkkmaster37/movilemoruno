import { Component, ElementRef, ViewChild } from '@angular/core';
import { AlertController, AnimationController, LoadingController, ModalController, NavController, ToastController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';
import { Router } from '@angular/router';
import { ViewModalPage } from '../view-modal/view-modal.page';
import { EditModalPage } from '../edit-modal/edit-modal.page';
import { CreatePagePage } from '../create-page/create-page.page';
import * as moment from 'moment';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  @ViewChild('loadingIcon', { read: ElementRef }) loadingIcon: ElementRef;
  urgencias: [];
  det: string;

  fecha:any;
  buttonOn: boolean;
  actual: string;
  constructor(
    private service: ServiceService,
    private animationCtrl: AnimationController,
    public loading: LoadingController,
    private modalController: ModalController,
    private alert: AlertController,
    public toast: ToastController,
    public router: Router
  ) {
    this.actual = moment().format('YYYY-MM-DD');
    this.getUrgencias();
  }

  ionViewDidEnter() {
    this.getUrgencias();
  }

  async getUrgencias(): Promise<void> {
    this.fecha = '';
    this.det = '';
    this.urgencias = [];
    this.buttonOn = true;

    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    this.animate();
    await loading.present().then(async () => {
      this.service.getEmergenciasDia().subscribe(async (data) => {
        this.urgencias = data.data;
        console.log(this.urgencias);
        
        this.det = 'lleno';
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });
    });
  }

  animate(): void {
    const loadingAnimation = this.animationCtrl.create('loading-animation')
      .addElement(this.loadingIcon.nativeElement)
      .duration(1500)
      .iterations(3)
      .fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
    loadingAnimation.play();
  }

  async viewEmergencia(id, item): Promise<void> {
    const modal = await this.modalController.create({
      component: ViewModalPage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        'id': id,
        'tipo': 'emergencia',
        'item': item
      }
    });
    return await modal.present();
  }

  async editEmergencia(item): Promise<void> {
    console.log(item);
    
    const modal = await this.modalController.create({
      component: EditModalPage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        'item': item,
        'tipo': 'emergencia'
      }
    });
    modal.onDidDismiss().then(() => {
      this.getUrgencias();
    });
    return await modal.present();
  }

  async deleteEmergencia(id): Promise<void> {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: '¿Eliminar Emergencia?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Okay',
          handler: () => {
            this.service.deleteEmergencia(id).subscribe((data) => {
              this.ok('Emergencia eliminada');
              this.getUrgencias();
            }, err => {
              this.error(err);
            })
          }
        }
      ]
    });
    await alert.present();
  }

  async createEmergencia(): Promise<void> {
    const modal = await this.modalController.create({
      component: CreatePagePage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
      componentProps: {
        'tipo': 'emergencia'
      }
    });
    modal.onDidDismiss().then(() => {
      this.getUrgencias();
    });
    return await modal.present();
  }

  async ok(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }

  async error(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  async searchFilterReserva(): Promise<void> {
    let fechaFilter = moment(this.fecha).format('YYYY-MM-DD');
    this.det = '';
    this.urgencias = [];
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    this.animate();
    await loading.present().then(async () => {
      this.service.searchReservaTratamiento(fechaFilter).subscribe(async (data) => {
        this.urgencias = data.data;
        this.det = 'lleno';
        loading.dismiss();
      }, err => {
        loading.dismiss();
      });
    });

  }

  reset(){
    this.buttonOn = false;
    this.fecha = '';
    this.getUrgencias();
  }

}
