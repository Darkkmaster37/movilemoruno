import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.page.html',
  styleUrls: ['./search-page.page.scss'],
})
export class SearchPagePage implements OnInit {
  pacientes: [];
  filterTerm: string;
  page: number;
  page_size: number;
  item:any;
  itemListData = [];
  page_number = 1;
  page_limit = 100;
  constructor(
    private service: ServiceService,
    public loading: LoadingController,
    private modal: ModalController
  ) {
   

   }

  ngOnInit() {
    this.getPacientes(false, "");    
  }

 
  async getPacientes(isFirstLoad, event): Promise<void> {
    // const loading = await this.loading.create({
    //   spinner: 'bubbles',
    //   translucent: true,
    // });
    // await loading.present().then(async () => {
      this.service.getPacientes(this.page_number, this.page_limit).subscribe((data: any) => {
        data = data.data;
        for (let i = 0; i < data.length; i++) {
          this.itemListData.push(data[i]);
        }

        if (isFirstLoad)
          event.target.complete();

        this.page_number++;
        // loading.present();

      }, error => {
        //  loading.present();

      })
  // })
}

  doInfinite(event) {
    this.getPacientes(true, event);
  }

  select(data) {
    this.modal.dismiss({
      id: data.id,
      nombres: data.nombres,
      apellidos: data.apellidos,
      control: true
    });
  }

  dismiss() {
    this.modal.dismiss({
      id: '',
      nombres: '',
      apellidos: '',
      control: false
    });
  }

  async searchProductos() {
    if (this.filterTerm === '') {
      this.page_number = 1;
      this.getPacientes(false, "");
    } else {
      this.itemListData = [];
      const loading = await this.loading.create({
        spinner: 'bubbles',
        translucent: true,
      });
      await loading.present().then(() => {
        this.service.searchProductos(this.filterTerm).subscribe(
          async data => {
            this.itemListData = data.data;            
            await loading.dismiss();
          }, err => {
            loading.dismiss();
          }
        );
      })
    }
    
  }

  
}
