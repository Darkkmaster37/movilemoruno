import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { ServiceService } from '../services/service.service';

@Component({
  selector: 'app-view-modal',
  templateUrl: './view-modal.page.html',
  styleUrls: ['./view-modal.page.scss'],
})
export class ViewModalPage implements OnInit {
  @Input() id: string;
  @Input() tipo: string;
  @Input() item: any;

  paciente = {nombres: 'Cargando...', apellidos: ''};
  reservas: any;
  reserva = {fecha: 'Cargando...', num_ficha: 'Cargando...', hora_ingreso: '', hora_llegada: '', tipo: '',tratamiento:'',paciente:{nombre:''}};
  clienteid: any;
  pacientes: any;
  tratamientos: any;
  tratamiento = {nombre: 'Cargando...', cantidad: 'Cargando...'};
  trataId: any;
  emergencias: any;
  emergencia = {fecha: 'Cargando...', hora: 'Cargando...', hora_ingreso: '', tipo: 'Cargando...',tratamiento:'',paciente:{nombre:''}};

  constructor(
    public modal: ModalController,
    private service: ServiceService,
    public loading: LoadingController
  ) { }

  async ngOnInit() {
    // this.getReserva();
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    await loading.dismiss();    
  }

  dismiss() {
    this.modal.dismiss();
  }

  async getReserva(): Promise<void> {
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    await loading.present().then(async () => {
      if (this.tipo == 'reserva') {
        this.service.getReservasDia().subscribe((data) => {
          this.reservas = data.data;
          this.reservas.filter(async (data) => {
            if (data.id == this.id) {
              this.reserva = data;
              this.clienteid = data.paciente_id;
              this.trataId = data.tratamiento_id;
              this.getPaciente(this.clienteid);
              this.getTratamiento(this.trataId);
              await loading.dismiss();
            }
          });
        }, err => {
          loading.dismiss();
        });
      } else if (this.tipo == 'emergencia') {
        this.service.getEmergenciasDia().subscribe((data) => {
          this.emergencias = data.data;
          this.emergencias.filter(async (data) => {
            if (data.id == this.id) {
              this.emergencia = data;
              this.clienteid = data.paciente_id;
              this.trataId = data.tratamiento_id;
              this.getPaciente(this.clienteid);
              this.getTratamiento(this.trataId);
              await loading.dismiss();
            }
          });
        }, err => {
          loading.dismiss();
        });
      }
    });
  }

  getPaciente(id): void {
    this.service.getPacientes(1,5000).subscribe((data) => {
      this.pacientes = data.data;
      this.pacientes.filter((data) => {
        if (data.id == id) {
          this.paciente = data;
        }
      });
    })
  }

  getTratamiento(id): void {
    this.service.getTratamientos().subscribe((data) => {
      this.tratamientos = data.data;
      this.tratamientos.filter((data) => {
        if (data.id == id) {
          this.tratamiento = data;
        }
      });
    })
  }

}
