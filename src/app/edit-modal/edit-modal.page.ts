import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, ModalController, ToastController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import * as moment from 'moment';
import { ServiceService } from '../services/service.service';
import { SearchPagePage } from '../search-page/search-page.page';
import { HorasPage } from '../horas/horas.page';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.page.html',
  styleUrls: ['./edit-modal.page.scss'],
})
export class EditModalPage implements OnInit {
  @Input() item: any;
  @Input() tipo: string;
  actual: any;
  ingreso: any;
  llegada: any;
  form: FormGroup;


  control: any;
  apellidos: any;
  nombres: any;
  conditionNombreNuevo = false;
  conditionHora = true;
  idPaciente: any;
  fechaLoad: any;
  controlHora: any;
  dataHour: any;
  fecha: any;
  constructor(
    private modal: ModalController,
    private loading: LoadingController,
    public toast: ToastController,
    public modalController: ModalController,
    public formB: FormBuilder,
    public alertController: AlertController,
    private service: ServiceService
  ) { }

  ngOnInit() {
    console.log(this.item);
    
    this.nombres = this.item.paciente.nombres;
    this.apellidos = this.item.paciente.apellidos;
    this.idPaciente = this.item.paciente.id;

    this.dataHour = this.item.hora;
    this.fecha = this.item.fecha;
    if (this.item) {
      this.control = true;
      this.controlHora = true;
    } else {
      this.control = false;
      this.controlHora = false;
    } 
  }

  dismiss() {
    this.modal.dismiss();
  }

  async searchCliente(): Promise<void> {
    this.conditionNombreNuevo = false;
    const modal = await this.modalController.create({
      component: SearchPagePage,
      cssClass: 'my-custom-class',
      swipeToClose: true,
    });
    await modal.present();
    const { data } = await modal.onDidDismiss();
    this.control = data.control;
    this.nombres = data.nombres;
    this.apellidos = data.apellidos;
    this.idPaciente = data.id;
  }

  async searchHora(): Promise<void> {
    if (this.fecha) {
      const modal = await this.modalController.create({
        component: HorasPage,
        cssClass: 'my-custom-class',
        swipeToClose: true,
        componentProps: {
          'fecha': this.fecha
        }
      });
      await modal.present();
      const { data } = await modal.onDidDismiss();
      if (data.dataEdit) {
        this.controlHora = data.controlHora;
        this.dataHour = data.data;
      } else {
        this.controlHora = true;
        this.fecha = this.item.fecha;
      }

    } else {
      this.presentAlertConfirm();

    }

  }
  eventDate(e) {
    this.item.fecha = moment(e.detail.value).format('YYYY-MM-DD');
    this.conditionHora = true;
  }
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      header: 'Alerta!',
      message: 'Se equivoco de hora tiene que seleccionar nuevamente la fecha!!!',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.form.reset()
          }
        }
      ]
    });

    await alert.present();
  }
  async edit(): Promise<void> {
    const loading = await this.loading.create({
      spinner: 'bubbles',
      translucent: true,
    });
    await loading.present().then(async () => {
      if (this.tipo == 'reserva') {
        this.service.editReserva(this.item.id,this.idPaciente, moment(this.fecha), this.dataHour).subscribe(async (data) => {
          this.ok('Reserva editada');
          loading.dismiss();
          this.dismiss();
        }, err => {
          this.error('Reserva no editada')
          loading.dismiss();
        });
      } else if (this.tipo == 'emergencia') {
        this.service.editEmergencia(this.item.id, this.idPaciente, moment(this.fecha), this.dataHour).subscribe(async (data) => {
          this.ok('Emergencia editada');
          this.dismiss();
          await loading.dismiss();
        }, err => {
          this.error('Emergencia no editada')
          loading.dismiss();
        });
      }
    });
  }
  async error(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

  async ok(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-success'
    });
    toast.present();
  }


}
