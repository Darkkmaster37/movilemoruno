import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  path = "https://c2100160.ferozo.com/api/user/";
  httpOptions: { headers: HttpHeaders; };

  constructor(private http: HttpClient) {

  }

  loginClient(credentials: { email, password }): Observable<any> {
    var datoaEnviar = {
      "email": credentials.email,
      "password": credentials.password,
    }
    return this.http.post(this.path + "login", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }
}
