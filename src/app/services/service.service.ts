import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  path = "https://c2100160.ferozo.com/api/user/";
  httpOptions: { headers: HttpHeaders; };

  constructor(private http: HttpClient) {

  }

  getPacientes(page, page_size): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.get(this.path + 'pacientes?page='+ page +'&page_size='+page_size+'', this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getTratamientos(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.get(this.path + "tratamientos", this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  getReservasDia(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.get(this.path + "reservas", this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  createReserva(id="", fecha, hora, tipo, ficha, observacion,tratamiento): Observable<any> {
    var datoaEnviar = {
      "paciente_id": id,
      "fecha": fecha,
      "hora": hora,
      "tipo": tipo,
      // "num_ficha": ficha,
      "observacion":observacion,
      "tratamiento_id": tratamiento
    }
    return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  editReserva(id, paciente_id,fecha,hora): Observable<any> {
    var datoaEnviar = {
      "reserva_id": id,
      // "hora_ingreso": hora_ingreso,
      // "hora_llegada": hora_llegada
      "paciente_id":paciente_id,
      "hora":hora,
      "fecha":fecha

    }
    return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  deleteReserva(id): Observable<any> {
    var datoaEnviar = {
      "reserva_id": id,
    }
    return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  getEmergenciasDia(): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };
    return this.http.get(this.path + "emergencias", this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        })
      );
  }

  createEmergencia(id='', fecha, hora,observacion, tratamiento): Observable<any> {
    var datoaEnviar = {
      "paciente_id": id,
      "fecha": fecha,
      "hora": hora,
      // "tipo": tipo,
      "observacion":observacion,
      "tratamiento_id": tratamiento
    }
    return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  editEmergencia(id, paciente_id,fecha,hora): Observable<any> {
    var datoaEnviar = {
      "emergencia_id": id,
      "paciente_id":paciente_id,
      "hora":hora,
      "fecha":fecha
    }
    return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  deleteEmergencia(id): Observable<any> {
    var datoaEnviar = {
      "emergencia_id": id,
    }
    return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  searchProductos(nombre): Observable<any> {
    var datoaEnviar = {
      "nombre": nombre,
    }
    return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }
  searchReserva(fecha): Observable<any> {
    var datoaEnviar = {
      "fecha": fecha,
    }
    return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

  searchReservaTratamiento(fecha): Observable<any> {
    var datoaEnviar = {
      "fecha": fecha,
    }
    return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions)
      .pipe(
        tap((data: any) => {
          return of(data);
        }),
        catchError((err) => {
          return throwError(err);
        }));
  }

}
