import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private nav: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (localStorage.getItem('id')) {
        this.nav.navigateRoot('/tabs/tab1');
      } else {
        this.nav.navigateRoot('/login');
      }
    });
  }
}
