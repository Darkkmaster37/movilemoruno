import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentials: FormGroup;

  constructor(
    private fb: FormBuilder,
    public loadingController: LoadingController,
    private authService: AuthService,
    public toast: ToastController,
    private nav: NavController,
  ) { }

  ngOnInit() {
    this.credentials = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  async login() {
    const loading = await this.loadingController.create({
      spinner: 'bubbles',
    });
    await loading.present();

    this.authService.loginClient(this.credentials.value).subscribe(
      async (res) => {
        localStorage.setItem('id', res.data.id);
        localStorage.setItem('username', res.data.username);
        localStorage.setItem('email', res.data.email);
        localStorage.setItem('foto', res.data.foto);
        localStorage.setItem('rol', res.data.rol);
        await loading.dismiss();
        this.nav.navigateRoot('/tabs/tab1');
      },
      async (res) => {
        console.log(res);
        
        await loading.dismiss();
        this.error(res.error.data.message);
      }
    );
  }

  get email() {
    return this.credentials.get('email');
  }

  get password() {
    return this.credentials.get('password');
  }

  async error(m) {
    const toast = await this.toast.create({
      message: m,
      duration: 3000,
      cssClass: 'my-custom-class-alert'
    });
    toast.present();
  }

}
