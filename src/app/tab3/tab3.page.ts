import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  foto: string;
  email: string;
  username: string;
  rol: string;
  id: string;
  none = 'https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png'

  constructor(
    private nav: NavController,
    public alertController:AlertController
  ) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.foto = localStorage.getItem('foto');
    this.email = localStorage.getItem('email');
    this.username = localStorage.getItem('username');
    this.id = localStorage.getItem('id');
    this.rol = localStorage.getItem('rol');
  }
  async logout (){
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Message <strong>Desea cerrar sesión?</strong>!!!',
      buttons: [
         {
          text: 'Si',
          handler: () => {
            localStorage.clear();
            this.nav.navigateRoot(['login']);
          }
        },
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ]
    });
  
    await alert.present();
  }
      

}
