(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~tab1-tab1-module~tab2-tab2-module~view-modal-view-modal-module"], {
    /***/
    "9Tgf":
    /*!***********************************************!*\
      !*** ./src/app/view-modal/view-modal.page.ts ***!
      \***********************************************/

    /*! exports provided: ViewModalPage */

    /***/
    function Tgf(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewModalPage", function () {
        return ViewModalPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_view_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./view-modal.page.html */
      "aBvg");
      /* harmony import */


      var _view_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./view-modal.page.scss */
      "gQTr");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../services/service.service */
      "rRxC");

      var ViewModalPage = /*#__PURE__*/function () {
        function ViewModalPage(modal, service, loading) {
          _classCallCheck(this, ViewModalPage);

          this.modal = modal;
          this.service = service;
          this.loading = loading;
          this.paciente = {
            nombres: 'Cargando...',
            apellidos: ''
          };
          this.reserva = {
            fecha: 'Cargando...',
            num_ficha: 'Cargando...',
            hora_ingreso: '',
            hora_llegada: '',
            tipo: '',
            tratamiento: '',
            paciente: {
              nombre: ''
            }
          };
          this.tratamiento = {
            nombre: 'Cargando...',
            cantidad: 'Cargando...'
          };
          this.emergencia = {
            fecha: 'Cargando...',
            hora: 'Cargando...',
            hora_ingreso: '',
            tipo: 'Cargando...',
            tratamiento: '',
            paciente: {
              nombre: ''
            }
          };
        }

        _createClass(ViewModalPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var loading;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 2:
                      loading = _context.sent;
                      _context.next = 5;
                      return loading.dismiss();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modal.dismiss();
          }
        }, {
          key: "getReserva",
          value: function getReserva() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 2:
                      loading = _context5.sent;
                      _context5.next = 5;
                      return loading.present().then(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                          var _this2 = this;

                          return regeneratorRuntime.wrap(function _callee4$(_context4) {
                            while (1) {
                              switch (_context4.prev = _context4.next) {
                                case 0:
                                  if (this.tipo == 'reserva') {
                                    this.service.getReservasDia().subscribe(function (data) {
                                      _this2.reservas = data.data;

                                      _this2.reservas.filter(function (data) {
                                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                            while (1) {
                                              switch (_context2.prev = _context2.next) {
                                                case 0:
                                                  if (!(data.id == this.id)) {
                                                    _context2.next = 8;
                                                    break;
                                                  }

                                                  this.reserva = data;
                                                  this.clienteid = data.paciente_id;
                                                  this.trataId = data.tratamiento_id;
                                                  this.getPaciente(this.clienteid);
                                                  this.getTratamiento(this.trataId);
                                                  _context2.next = 8;
                                                  return loading.dismiss();

                                                case 8:
                                                case "end":
                                                  return _context2.stop();
                                              }
                                            }
                                          }, _callee2, this);
                                        }));
                                      });
                                    }, function (err) {
                                      loading.dismiss();
                                    });
                                  } else if (this.tipo == 'emergencia') {
                                    this.service.getEmergenciasDia().subscribe(function (data) {
                                      _this2.emergencias = data.data;

                                      _this2.emergencias.filter(function (data) {
                                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                                          return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                            while (1) {
                                              switch (_context3.prev = _context3.next) {
                                                case 0:
                                                  if (!(data.id == this.id)) {
                                                    _context3.next = 8;
                                                    break;
                                                  }

                                                  this.emergencia = data;
                                                  this.clienteid = data.paciente_id;
                                                  this.trataId = data.tratamiento_id;
                                                  this.getPaciente(this.clienteid);
                                                  this.getTratamiento(this.trataId);
                                                  _context3.next = 8;
                                                  return loading.dismiss();

                                                case 8:
                                                case "end":
                                                  return _context3.stop();
                                              }
                                            }
                                          }, _callee3, this);
                                        }));
                                      });
                                    }, function (err) {
                                      loading.dismiss();
                                    });
                                  }

                                case 1:
                                case "end":
                                  return _context4.stop();
                              }
                            }
                          }, _callee4, this);
                        }));
                      });

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "getPaciente",
          value: function getPaciente(id) {
            var _this3 = this;

            this.service.getPacientes(1, 5000).subscribe(function (data) {
              _this3.pacientes = data.data;

              _this3.pacientes.filter(function (data) {
                if (data.id == id) {
                  _this3.paciente = data;
                }
              });
            });
          }
        }, {
          key: "getTratamiento",
          value: function getTratamiento(id) {
            var _this4 = this;

            this.service.getTratamientos().subscribe(function (data) {
              _this4.tratamientos = data.data;

              _this4.tratamientos.filter(function (data) {
                if (data.id == id) {
                  _this4.tratamiento = data;
                }
              });
            });
          }
        }]);

        return ViewModalPage;
      }();

      ViewModalPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }];
      };

      ViewModalPage.propDecorators = {
        id: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        tipo: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        item: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }]
      };
      ViewModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-view-modal',
        template: _raw_loader_view_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_view_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], ViewModalPage);
      /***/
    },

    /***/
    "aBvg":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/view-modal/view-modal.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function aBvg(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header *ngIf=\"tipo == 'reserva'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Reservas del día #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'reserva'\" class=\"ion-padding\">\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <div class=\"ion-text-center\">\r\n          <strong>NUM FICHA </strong>{{item.num_ficha}}\r\n\r\n      </div>\r\n      <ion-item>\r\n        <ion-avatar slot=\"start\">\r\n          <img src=\"assets/user.png\" />\r\n        </ion-avatar>\r\n        <ion-label>\r\n        <p>{{item.paciente.nombre}}</p>\r\n        <p>Tipo: {{item.tipo}}</p>\r\n        </ion-label>\r\n      </ion-item>\r\n      <br>\r\n      <ion-grid>\r\n          <ion-row>\r\n              <ion-col size=\"6\">\r\n               <b>Fecha de Reserva</b> \r\n              <p>{{item.fecha}}</p>  \r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                  <b>Tipo de tratamiento</b> \r\n  \r\n              <p>  {{item.tratamiento}}</p>\r\n  \r\n              </ion-col>\r\n            </ion-row>\r\n        <!-- <ion-row>\r\n          <ion-col class=\"ion-text-center\">\r\n            <br>\r\n            <strong>FECHA : </strong>{{reserva.fecha}}\r\n            <br>\r\n            <strong>FICHA : </strong>{{reserva.num_ficha}}\r\n            <br>\r\n            <div *ngIf=\"reserva.hora_ingreso\">\r\n              <strong>HORA DE INGRESO : </strong>{{reserva.hora_ingreso}}\r\n            </div>\r\n            <div *ngIf=\"reserva.hora_llegada\">\r\n              <strong>HORA DE LLEGADA : </strong>{{reserva.hora_llegada}}\r\n            </div>\r\n            <strong>CLIENTE : </strong>{{paciente.nombres}} {{' ' + paciente.apellidos}}\r\n            <br>\r\n            <strong>TIPO : </strong>{{reserva.tipo}}\r\n            <br>\r\n            <strong>TRATAMIENTO : </strong>{{tratamiento.nombre | titlecase}}\r\n            <br>\r\n            <strong>CANTIDAD : </strong>{{tratamiento.cantidad}} unidade(s)\r\n          </ion-col>\r\n        </ion-row> -->\r\n        <ion-row>\r\n          <ion-col class=\"ion-text-center margin\">\r\n            <ion-button (click)=\"dismiss()\">Cerrar</ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n</ion-content>\r\n\r\n<ion-header *ngIf=\"tipo == 'emergencia'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Emergencia del día #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'emergencia'\" class=\"ion-padding\">\r\n  <ion-card>\r\n    <ion-card-content>\r\n\r\n      <ion-grid>\r\n          <ion-item>\r\n              <ion-avatar slot=\"start\">\r\n                <img src=\"assets/user.png\" />\r\n              </ion-avatar>\r\n              <ion-label>\r\n              <p>{{item.paciente.nombre}}</p>\r\n              <p>Tipo: {{item.tipo}}</p>\r\n              </ion-label>\r\n            </ion-item>\r\n            <ion-row>\r\n                <ion-col size=\"6\">\r\n                 <b>Fecha de Reserva</b> \r\n                <p>{{item.fecha}}</p>  \r\n                </ion-col>\r\n                <ion-col size=\"6\">\r\n                    <b>Tipo de tratamiento</b> \r\n    \r\n                <p>  {{item.tratamiento}}</p>\r\n    \r\n                </ion-col>\r\n              </ion-row>\r\n          <!-- <ion-col class=\"ion-text-center\">\r\n            <br>\r\n            <strong>FECHA : </strong>{{emergencia.fecha}}\r\n            <br>\r\n            <strong>HORA : </strong>{{emergencia.hora}}\r\n            <br>\r\n            <div *ngIf=\"emergencia.hora_ingreso\">\r\n              <strong>HORA DE INGRESO : </strong>{{emergencia.hora_ingreso}}\r\n            </div>\r\n            <strong>CLIENTE : </strong>{{paciente.nombres}} {{' ' + paciente.apellidos}}\r\n            <br>\r\n            <strong>TIPO : </strong>{{emergencia.tipo}}\r\n            <br>\r\n            <strong>TRATAMIENTO : </strong>{{tratamiento.nombre | titlecase}}\r\n            <br>\r\n            <strong>CANTIDAD : </strong>{{tratamiento.cantidad}} unidade(s)\r\n          </ion-col> -->\r\n        <ion-row>\r\n          <ion-col class=\"ion-text-center margin\">\r\n            <ion-button (click)=\"dismiss()\">Cerrar</ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n</ion-content>";
      /***/
    },

    /***/
    "gQTr":
    /*!*************************************************!*\
      !*** ./src/app/view-modal/view-modal.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function gQTr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".margin {\n  margin-top: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHZpZXctbW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJ2aWV3LW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXJnaW4ge1xyXG4gICAgbWFyZ2luLXRvcDogMTUwcHg7XHJcbn0iXX0= */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~tab1-tab1-module~tab2-tab2-module~view-modal-view-modal-module-es5.js.map