(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~create-page-create-page-module~edit-modal-edit-modal-module~search-page-search-page-module~t~b5c43584"],{

/***/ "AVkp":
/*!***************************************************!*\
  !*** ./src/app/search-page/search-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#add {\n  color: #27ae60;\n}\n\n.buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHNlYXJjaC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNlYXJjaC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhZGQge1xyXG4gICAgY29sb3I6ICMyN2FlNjA7XHJcbn1cclxuXHJcbi5idXR0b25zIHtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "kUx7":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search-page/search-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>Buscar cliente</ion-title>\r\n \r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-searchbar placeholder=\"Buscar...\" [(ngModel)]=\"filterTerm\" animated=\"true\" [debounce]=\"2000\"\r\n(ionChange)=\"searchProductos()\"\r\n[autocomplete]=\"true\"\r\n\r\n></ion-searchbar>\r\n<ion-content>\r\n\r\n\r\n  <ion-list>\r\n    <ion-item *ngFor=\"let data of itemListData \" (click)=\"select(data)\">\r\n      <ion-label>{{( data.nombres | titlecase) + ' ' + (data.apellidos | titlecase)}}</ion-label>\r\n      <ion-icon id=\"add\" (click)=\"select(data)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\r\n    </ion-item>\r\n    <!-- <ion-virtual-scroll [items]=\"pacientes\" approxItemHeight=\"320px\">\r\n        <ion-card *virtualItem=\"let item;\">\r\n            <ion-item (click)=\"select(item)\">\r\n                <ion-label>{{(item.apellidos | titlecase) + ' ' + (item.nombres | titlecase)}}</ion-label>\r\n                <ion-icon id=\"add\" (click)=\"select(item)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\r\n              </ion-item>\r\n        </ion-card>\r\n      </ion-virtual-scroll> -->\r\n    \r\n  </ion-list>\r\n  <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\r\n      <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n      </ion-infinite-scroll-content>\r\n    </ion-infinite-scroll>\r\n</ion-content>");

/***/ }),

/***/ "rRxC":
/*!*********************************************!*\
  !*** ./src/app/services/service.service.ts ***!
  \*********************************************/
/*! exports provided: ServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceService", function() { return ServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let ServiceService = class ServiceService {
    constructor(http) {
        this.http = http;
        this.path = "https://c2100160.ferozo.com/api/user/";
    }
    getPacientes(page, page_size) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + 'pacientes?page=' + page + '&page_size=' + page_size + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getTratamientos() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "tratamientos", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getReservasDia() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "reservas", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    createReserva(id = "", fecha, hora, tipo, ficha, observacion, tratamiento) {
        var datoaEnviar = {
            "paciente_id": id,
            "fecha": fecha,
            "hora": hora,
            "tipo": tipo,
            // "num_ficha": ficha,
            "observacion": observacion,
            "tratamiento_id": tratamiento
        };
        return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editReserva(id, paciente_id, fecha, hora) {
        var datoaEnviar = {
            "reserva_id": id,
            // "hora_ingreso": hora_ingreso,
            // "hora_llegada": hora_llegada
            "paciente_id": paciente_id,
            "hora": hora,
            "fecha": fecha
        };
        return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    deleteReserva(id) {
        var datoaEnviar = {
            "reserva_id": id,
        };
        return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getEmergenciasDia() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "emergencias", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    createEmergencia(id = '', fecha, hora, observacion, tratamiento) {
        var datoaEnviar = {
            "paciente_id": id,
            "fecha": fecha,
            "hora": hora,
            // "tipo": tipo,
            "observacion": observacion,
            "tratamiento_id": tratamiento
        };
        return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editEmergencia(id, paciente_id, fecha, hora) {
        var datoaEnviar = {
            "emergencia_id": id,
            "paciente_id": paciente_id,
            "hora": hora,
            "fecha": fecha
        };
        return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    deleteEmergencia(id) {
        var datoaEnviar = {
            "emergencia_id": id,
        };
        return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchProductos(nombre) {
        var datoaEnviar = {
            "nombre": nombre,
        };
        return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchReserva(fecha) {
        var datoaEnviar = {
            "fecha": fecha,
        };
        return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchReservaTratamiento(fecha) {
        var datoaEnviar = {
            "fecha": fecha,
        };
        return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ServiceService);



/***/ }),

/***/ "tw2k":
/*!*************************************************!*\
  !*** ./src/app/search-page/search-page.page.ts ***!
  \*************************************************/
/*! exports provided: SearchPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPagePage", function() { return SearchPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./search-page.page.html */ "kUx7");
/* harmony import */ var _search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-page.page.scss */ "AVkp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");






let SearchPagePage = class SearchPagePage {
    constructor(service, loading, modal) {
        this.service = service;
        this.loading = loading;
        this.modal = modal;
        this.itemListData = [];
        this.page_number = 1;
        this.page_limit = 100;
    }
    ngOnInit() {
        this.getPacientes(false, "");
    }
    getPacientes(isFirstLoad, event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // const loading = await this.loading.create({
            //   spinner: 'bubbles',
            //   translucent: true,
            // });
            // await loading.present().then(async () => {
            this.service.getPacientes(this.page_number, this.page_limit).subscribe((data) => {
                data = data.data;
                for (let i = 0; i < data.length; i++) {
                    this.itemListData.push(data[i]);
                }
                if (isFirstLoad)
                    event.target.complete();
                this.page_number++;
                // loading.present();
            }, error => {
                //  loading.present();
            });
            // })
        });
    }
    doInfinite(event) {
        this.getPacientes(true, event);
    }
    select(data) {
        this.modal.dismiss({
            id: data.id,
            nombres: data.nombres,
            apellidos: data.apellidos,
            control: true
        });
    }
    dismiss() {
        this.modal.dismiss({
            id: '',
            nombres: '',
            apellidos: '',
            control: false
        });
    }
    searchProductos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.filterTerm === '') {
                this.page_number = 1;
                this.getPacientes(false, "");
            }
            else {
                this.itemListData = [];
                const loading = yield this.loading.create({
                    spinner: 'bubbles',
                    translucent: true,
                });
                yield loading.present().then(() => {
                    this.service.searchProductos(this.filterTerm).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.itemListData = data.data;
                        yield loading.dismiss();
                    }), err => {
                        loading.dismiss();
                    });
                });
            }
        });
    }
};
SearchPagePage.ctorParameters = () => [
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
SearchPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-search-page',
        template: _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SearchPagePage);



/***/ })

}]);
//# sourceMappingURL=default~create-page-create-page-module~edit-modal-edit-modal-module~search-page-search-page-module~t~b5c43584-es2015.js.map