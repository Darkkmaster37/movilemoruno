(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab2-tab2-module"], {
    /***/
    "EGAO":
    /*!*************************************!*\
      !*** ./src/app/tab2/tab2.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function EGAO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n\nion-item-option {\n  background: white;\n}\n\n.label-date {\n  color: black;\n}\n\nimg {\n  border-radius: 50px;\n  width: 50px;\n  height: 50px;\n}\n\n#name {\n  font-size: 0.7em;\n  color: black;\n}\n\n.title-span {\n  font-size: 0.7em;\n}\n\nion-row {\n  width: 100%;\n  background: white;\n}\n\nion-input[type=date]:before {\n  content: attr(placeholder) !important;\n  color: #aaa;\n  margin-right: 0.5em;\n}\n\ninput[type=date]:focus:before,\ninput[type=date]:valid:before {\n  content: \"\";\n}\n\n#add {\n  color: #27ae60;\n}\n\n#edit-menu {\n  color: #2980b9;\n}\n\n#eliminar {\n  color: #c0392b;\n}\n\nion-list {\n  padding-top: 30px;\n}\n\n.text {\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.danger {\n  color: red;\n  font-weight: bolder;\n}\n\n.margin {\n  margin-top: 25px;\n}\n\n.label-date {\n  color: black;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRhYjIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxpQkFBQTtBQUVKOztBQUFBO0VBQ0ksWUFBQTtBQUdKOztBQURBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUlKOztBQUZBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0FBS0o7O0FBSEE7RUFDSSxnQkFBQTtBQU1KOztBQUpBO0VBQ0ksV0FBQTtFQUNBLGlCQUFBO0FBT0o7O0FBRUE7RUFDSSxxQ0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNFOztFQUVFLFdBQUE7QUFFSjs7QUFBQTtFQUNJLGNBQUE7QUFHSjs7QUFEQTtFQUNJLGNBQUE7QUFJSjs7QUFGQTtFQUNJLGNBQUE7QUFLSjs7QUFIQTtFQUNJLGlCQUFBO0FBTUo7O0FBSkE7RUFDSSxxQkFBQTtFQUNBLHNCQUFBO0FBT0o7O0FBTEE7RUFDSSxVQUFBO0VBQ0EsbUJBQUE7QUFRSjs7QUFMQTtFQUNJLGdCQUFBO0FBUUo7O0FBTkE7RUFDSSxZQUFBO0FBU0oiLCJmaWxlIjoidGFiMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnV0dG9ucyB7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG5pb24taXRlbS1vcHRpb257XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxufVxyXG4ubGFiZWwtZGF0ZXtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG5pbWd7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuI25hbWV7XHJcbiAgICBmb250LXNpemU6IDAuN2VtO1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi50aXRsZS1zcGFue1xyXG4gICAgZm9udC1zaXplOiAwLjdlbTtcclxufVxyXG5pb24tcm93e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuXHJcbn1cclxuXHJcbi8vICNpbnB1dC1kYXRle1xyXG4vLyAgICAgLS1jb2xvcjogd2hpdGU7XHJcbi8vICAgICBiYWNrZ3JvdW5kOiAjMzg4MGZmO1xyXG4vLyAgICAgLy8gYm9yZGVyOiBub25lO1xyXG4vLyB9XHJcbmlvbi1pbnB1dFt0eXBlPVwiZGF0ZVwiXTpiZWZvcmUge1xyXG4gICAgY29udGVudDogYXR0cihwbGFjZWhvbGRlcikgIWltcG9ydGFudDtcclxuICAgIGNvbG9yOiAjYWFhO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwLjVlbTtcclxuICB9XHJcbiAgaW5wdXRbdHlwZT1cImRhdGVcIl06Zm9jdXM6YmVmb3JlLFxyXG4gIGlucHV0W3R5cGU9XCJkYXRlXCJdOnZhbGlkOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gIH1cclxuI2FkZCB7XHJcbiAgICBjb2xvcjogIzI3YWU2MDtcclxufVxyXG4jZWRpdC1tZW51IHtcclxuICAgIGNvbG9yOiAjMjk4MGI5O1xyXG59XHJcbiNlbGltaW5hciB7XHJcbiAgICBjb2xvcjogI2MwMzkyYjtcclxufVxyXG5pb24tbGlzdCB7XHJcbiAgICBwYWRkaW5nLXRvcDogMzBweDtcclxufVxyXG4udGV4dCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcbi5kYW5nZXIge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbn1cclxuXHJcbi5tYXJnaW4ge1xyXG4gICAgbWFyZ2luLXRvcDogMjVweDtcclxufVxyXG4ubGFiZWwtZGF0ZXtcclxuICAgIGNvbG9yOiBibGFjaztcclxufVxyXG4iXX0= */";
      /***/
    },

    /***/
    "JZ9U":
    /*!***********************************!*\
      !*** ./src/app/tab2/tab2.page.ts ***!
      \***********************************/

    /*! exports provided: Tab2Page */

    /***/
    function JZ9U(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2Page", function () {
        return Tab2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_tab2_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./tab2.page.html */
      "e9nj");
      /* harmony import */


      var _tab2_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./tab2.page.scss */
      "EGAO");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../services/service.service */
      "rRxC");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _view_modal_view_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../view-modal/view-modal.page */
      "9Tgf");
      /* harmony import */


      var _edit_modal_edit_modal_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../edit-modal/edit-modal.page */
      "4Hae");
      /* harmony import */


      var _create_page_create_page_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../create-page/create-page.page */
      "LXxN");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! moment */
      "wd/R");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);

      var Tab2Page = /*#__PURE__*/function () {
        function Tab2Page(service, animationCtrl, loading, modalController, alert, toast, router) {
          _classCallCheck(this, Tab2Page);

          this.service = service;
          this.animationCtrl = animationCtrl;
          this.loading = loading;
          this.modalController = modalController;
          this.alert = alert;
          this.toast = toast;
          this.router = router;
          this.actual = moment__WEBPACK_IMPORTED_MODULE_10__().format('YYYY-MM-DD');
          this.getUrgencias();
        }

        _createClass(Tab2Page, [{
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.getUrgencias();
          }
        }, {
          key: "getUrgencias",
          value: function getUrgencias() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.fecha = '';
                      this.det = '';
                      this.urgencias = [];
                      this.buttonOn = true;
                      _context3.next = 6;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 6:
                      loading = _context3.sent;
                      this.animate();
                      _context3.next = 10;
                      return loading.present().then(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                          var _this2 = this;

                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  this.service.getEmergenciasDia().subscribe(function (data) {
                                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                      return regeneratorRuntime.wrap(function _callee$(_context) {
                                        while (1) {
                                          switch (_context.prev = _context.next) {
                                            case 0:
                                              this.urgencias = data.data;
                                              console.log(this.urgencias);
                                              this.det = 'lleno';
                                              loading.dismiss();

                                            case 4:
                                            case "end":
                                              return _context.stop();
                                          }
                                        }
                                      }, _callee, this);
                                    }));
                                  }, function (err) {
                                    loading.dismiss();
                                  });

                                case 1:
                                case "end":
                                  return _context2.stop();
                              }
                            }
                          }, _callee2, this);
                        }));
                      });

                    case 10:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "animate",
          value: function animate() {
            var loadingAnimation = this.animationCtrl.create('loading-animation').addElement(this.loadingIcon.nativeElement).duration(1500).iterations(3).fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
            loadingAnimation.play();
          }
        }, {
          key: "viewEmergencia",
          value: function viewEmergencia(id, item) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var modal;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.modalController.create({
                        component: _view_modal_view_modal_page__WEBPACK_IMPORTED_MODULE_7__["ViewModalPage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'id': id,
                          'tipo': 'emergencia',
                          'item': item
                        }
                      });

                    case 2:
                      modal = _context4.sent;
                      _context4.next = 5;
                      return modal.present();

                    case 5:
                      return _context4.abrupt("return", _context4.sent);

                    case 6:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "editEmergencia",
          value: function editEmergencia(item) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this3 = this;

              var modal;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      console.log(item);
                      _context5.next = 3;
                      return this.modalController.create({
                        component: _edit_modal_edit_modal_page__WEBPACK_IMPORTED_MODULE_8__["EditModalPage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'item': item,
                          'tipo': 'emergencia'
                        }
                      });

                    case 3:
                      modal = _context5.sent;
                      modal.onDidDismiss().then(function () {
                        _this3.getUrgencias();
                      });
                      _context5.next = 7;
                      return modal.present();

                    case 7:
                      return _context5.abrupt("return", _context5.sent);

                    case 8:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "deleteEmergencia",
          value: function deleteEmergencia(id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this4 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.alert.create({
                        cssClass: 'my-custom-class',
                        header: 'Confirm!',
                        message: '¿Eliminar Emergencia?',
                        buttons: [{
                          text: 'Cancel',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler(blah) {}
                        }, {
                          text: 'Okay',
                          handler: function handler() {
                            _this4.service.deleteEmergencia(id).subscribe(function (data) {
                              _this4.ok('Emergencia eliminada');

                              _this4.getUrgencias();
                            }, function (err) {
                              _this4.error(err);
                            });
                          }
                        }]
                      });

                    case 2:
                      alert = _context6.sent;
                      _context6.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "createEmergencia",
          value: function createEmergencia() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this5 = this;

              var modal;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.modalController.create({
                        component: _create_page_create_page_page__WEBPACK_IMPORTED_MODULE_9__["CreatePagePage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'tipo': 'emergencia'
                        }
                      });

                    case 2:
                      modal = _context7.sent;
                      modal.onDidDismiss().then(function () {
                        _this5.getUrgencias();
                      });
                      _context7.next = 6;
                      return modal.present();

                    case 6:
                      return _context7.abrupt("return", _context7.sent);

                    case 7:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "ok",
          value: function ok(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var toast;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      _context8.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 3000,
                        cssClass: 'my-custom-class-success'
                      });

                    case 2:
                      toast = _context8.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }, {
          key: "error",
          value: function error(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var toast;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 3000,
                        cssClass: 'my-custom-class-alert'
                      });

                    case 2:
                      toast = _context9.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "searchFilterReserva",
          value: function searchFilterReserva() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
              var _this6 = this;

              var fechaFilter, loading;
              return regeneratorRuntime.wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      fechaFilter = moment__WEBPACK_IMPORTED_MODULE_10__(this.fecha).format('YYYY-MM-DD');
                      this.det = '';
                      this.urgencias = [];
                      _context12.next = 5;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 5:
                      loading = _context12.sent;
                      this.animate();
                      _context12.next = 9;
                      return loading.present().then(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this6, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
                          var _this7 = this;

                          return regeneratorRuntime.wrap(function _callee11$(_context11) {
                            while (1) {
                              switch (_context11.prev = _context11.next) {
                                case 0:
                                  this.service.searchReservaTratamiento(fechaFilter).subscribe(function (data) {
                                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this7, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                                      return regeneratorRuntime.wrap(function _callee10$(_context10) {
                                        while (1) {
                                          switch (_context10.prev = _context10.next) {
                                            case 0:
                                              this.urgencias = data.data;
                                              this.det = 'lleno';
                                              loading.dismiss();

                                            case 3:
                                            case "end":
                                              return _context10.stop();
                                          }
                                        }
                                      }, _callee10, this);
                                    }));
                                  }, function (err) {
                                    loading.dismiss();
                                  });

                                case 1:
                                case "end":
                                  return _context11.stop();
                              }
                            }
                          }, _callee11, this);
                        }));
                      });

                    case 9:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12, this);
            }));
          }
        }, {
          key: "reset",
          value: function reset() {
            this.buttonOn = false;
            this.fecha = '';
            this.getUrgencias();
          }
        }]);

        return Tab2Page;
      }();

      Tab2Page.ctorParameters = function () {
        return [{
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AnimationController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
        }];
      };

      Tab2Page.propDecorators = {
        loadingIcon: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['loadingIcon', {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
          }]
        }]
      };
      Tab2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab2',
        template: _raw_loader_tab2_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab2_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], Tab2Page);
      /***/
    },

    /***/
    "TUkU":
    /*!*************************************!*\
      !*** ./src/app/tab2/tab2.module.ts ***!
      \*************************************/

    /*! exports provided: Tab2PageModule */

    /***/
    function TUkU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2PageModule", function () {
        return Tab2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tab2.page */
      "JZ9U");
      /* harmony import */


      var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../explore-container/explore-container.module */
      "qtYk");
      /* harmony import */


      var _tab2_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./tab2-routing.module */
      "UDmF");

      var Tab2PageModule = function Tab2PageModule() {
        _classCallCheck(this, Tab2PageModule);
      };

      Tab2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"], _tab2_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab2PageRoutingModule"]],
        declarations: [_tab2_page__WEBPACK_IMPORTED_MODULE_5__["Tab2Page"]]
      })], Tab2PageModule);
      /***/
    },

    /***/
    "UDmF":
    /*!*********************************************!*\
      !*** ./src/app/tab2/tab2-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: Tab2PageRoutingModule */

    /***/
    function UDmF(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab2PageRoutingModule", function () {
        return Tab2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _tab2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tab2.page */
      "JZ9U");

      var routes = [{
        path: '',
        component: _tab2_page__WEBPACK_IMPORTED_MODULE_3__["Tab2Page"]
      }];

      var Tab2PageRoutingModule = function Tab2PageRoutingModule() {
        _classCallCheck(this, Tab2PageRoutingModule);
      };

      Tab2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Tab2PageRoutingModule);
      /***/
    },

    /***/
    "e9nj":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab2/tab2.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function e9nj(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <br>\r\n    <ion-title class=\"ion-text-center\">Tratamientos del día</ion-title>\r\n    <!-- <ion-button (click)=\"createEmergencia()\" class=\"margin\" expand=\"block\" color=\"success\">\r\n        Crear Emergencia\r\n        <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\r\n      </ion-button> -->\r\n      <ion-buttons slot=\"end\">\r\n          <ion-button (click)=\"createEmergencia()\" color=\"light\">\r\n            <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"add\"></ion-icon>\r\n          </ion-button>\r\n        </ion-buttons>\r\n  </ion-toolbar>\r\n  <ion-row>\r\n    <ion-col  class=\"ion-text-center\"  size=\"6\">\r\n      <br>\r\n      <ion-label class=\"label-date\">Fecha de Reservas</ion-label>\r\n      <!-- <ion-input id=\"input-date\" type=\"date\" [(ngModel)]=\"fecha\" placeholder=\"DD/MM/YYYY\"></ion-input> -->\r\n      <input min=\"{{actual}}\" type=\"date\"  [(ngModel)]=\"fecha\" />\r\n\r\n      <!-- <ion-datetime [(ngModel)]=\"fecha\" display-format=\"DD-MM-YYYY\" placeholder=\"DD/MM/YYYY\"></ion-datetime> -->\r\n    </ion-col>\r\n    <ion-col size=\"6\">\r\n        <ion-button (click)=\"searchFilterReserva()\" expand=\"block\"  shape=\"round\">\r\n            Buscar\r\n          </ion-button>\r\n          <p *ngIf=\"buttonOn\" >\r\n          <ion-button (click)=\"reset()\" expand=\"block\" shape=\"round\">\r\n              Limpiar\r\n            </ion-button>\r\n          </p>\r\n    </ion-col>\r\n  </ion-row>\r\n</ion-header>\r\n\r\n<ion-content >\r\n\r\n\r\n  <ion-list *ngIf=\"urgencias.length == 0 && !det\">\r\n    <ion-button expand=\"block\">\r\n      Cargando\r\n      <ion-icon name=\"reload\" style=\"font-size: 25px;\" slot=\"end\" #loadingIcon></ion-icon>\r\n    </ion-button>\r\n  </ion-list>\r\n\r\n  <ion-list *ngIf=\"urgencias.length == 0 && det\">\r\n    <ion-item>\r\n      <span class=\"text danger\">No hay tratamientos ambulatorios </span>\r\n    </ion-item>\r\n  </ion-list>\r\n\r\n\r\n    <ion-list *ngIf=\"urgencias.length >= 1 && det\">\r\n        <ion-item-sliding *ngFor=\"let item of urgencias\" >\r\n            <ion-item>\r\n              <ion-row>\r\n                  <ion-col size=\"12\">\r\n                      <ion-label>\r\n                          <p id=\"name\"><b>{{item.paciente.nombre}}</b> </p>\r\n                          <p id=\"name\" *ngIf=\"item.observacion\" ><b>{{item.observacion}}</b> </p>\r\n                          <ion-row>\r\n                            <ion-col size=\"6\">\r\n                                <p>{{item.hora}}</p>\r\n                            </ion-col>\r\n                            <ion-col  size=\"6\">\r\n                                <p >Tipo: <span class=\"title-span\">{{item.tratamiento}}</span> </p> \r\n                              </ion-col>\r\n                          </ion-row>\r\n                      </ion-label>\r\n                    </ion-col>\r\n              </ion-row>\r\n            <ion-note slot=\"end\">\r\n              Deslizar\r\n            </ion-note>\r\n          </ion-item>\r\n          <ion-item-options side=\"end\">\r\n            <ion-item-option (click)=\"viewEmergencia(item.id,item)\">\r\n              <ion-icon id=\"add\"  slot=\"icon-only\" name=\"eye\"></ion-icon>\r\n            </ion-item-option>\r\n            <ion-item-option (click)=\"editEmergencia(item)\">\r\n              <ion-icon id=\"edit-menu\"  slot=\"icon-only\" name=\"create\"></ion-icon>\r\n            </ion-item-option>\r\n            <ion-item-option (click)=\"deleteEmergencia(item.id)\">\r\n              <ion-icon id=\"eliminar\"  slot=\"icon-only\" name=\"trash\"></ion-icon>\r\n            </ion-item-option>\r\n          </ion-item-options>\r\n      \r\n        </ion-item-sliding>\r\n      </ion-list>\r\n</ion-content>\r\n";
      /***/
    }
  }]);
})();
//# sourceMappingURL=tab2-tab2-module-es5.js.map