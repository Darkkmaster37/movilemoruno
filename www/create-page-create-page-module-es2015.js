(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["create-page-create-page-module"],{

/***/ "P1ZU":
/*!***********************************************************!*\
  !*** ./src/app/create-page/create-page-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: CreatePagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePagePageRoutingModule", function() { return CreatePagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _create_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./create-page.page */ "LXxN");




const routes = [
    {
        path: '',
        component: _create_page_page__WEBPACK_IMPORTED_MODULE_3__["CreatePagePage"]
    }
];
let CreatePagePageRoutingModule = class CreatePagePageRoutingModule {
};
CreatePagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CreatePagePageRoutingModule);



/***/ }),

/***/ "X7Zy":
/*!***************************************************!*\
  !*** ./src/app/create-page/create-page.module.ts ***!
  \***************************************************/
/*! exports provided: CreatePagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePagePageModule", function() { return CreatePagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _create_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./create-page-routing.module */ "P1ZU");
/* harmony import */ var _create_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./create-page.page */ "LXxN");







let CreatePagePageModule = class CreatePagePageModule {
};
CreatePagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _create_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["CreatePagePageRoutingModule"]
        ],
        declarations: [_create_page_page__WEBPACK_IMPORTED_MODULE_6__["CreatePagePage"]]
    })
], CreatePagePageModule);



/***/ })

}]);
//# sourceMappingURL=create-page-create-page-module-es2015.js.map