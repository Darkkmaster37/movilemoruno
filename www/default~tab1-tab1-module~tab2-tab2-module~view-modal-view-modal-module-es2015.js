(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~tab1-tab1-module~tab2-tab2-module~view-modal-view-modal-module"],{

/***/ "9Tgf":
/*!***********************************************!*\
  !*** ./src/app/view-modal/view-modal.page.ts ***!
  \***********************************************/
/*! exports provided: ViewModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewModalPage", function() { return ViewModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_view_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./view-modal.page.html */ "aBvg");
/* harmony import */ var _view_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./view-modal.page.scss */ "gQTr");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");






let ViewModalPage = class ViewModalPage {
    constructor(modal, service, loading) {
        this.modal = modal;
        this.service = service;
        this.loading = loading;
        this.paciente = { nombres: 'Cargando...', apellidos: '' };
        this.reserva = { fecha: 'Cargando...', num_ficha: 'Cargando...', hora_ingreso: '', hora_llegada: '', tipo: '', tratamiento: '', paciente: { nombre: '' } };
        this.tratamiento = { nombre: 'Cargando...', cantidad: 'Cargando...' };
        this.emergencia = { fecha: 'Cargando...', hora: 'Cargando...', hora_ingreso: '', tipo: 'Cargando...', tratamiento: '', paciente: { nombre: '' } };
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // this.getReserva();
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            yield loading.dismiss();
        });
    }
    dismiss() {
        this.modal.dismiss();
    }
    getReserva() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            yield loading.present().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                if (this.tipo == 'reserva') {
                    this.service.getReservasDia().subscribe((data) => {
                        this.reservas = data.data;
                        this.reservas.filter((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            if (data.id == this.id) {
                                this.reserva = data;
                                this.clienteid = data.paciente_id;
                                this.trataId = data.tratamiento_id;
                                this.getPaciente(this.clienteid);
                                this.getTratamiento(this.trataId);
                                yield loading.dismiss();
                            }
                        }));
                    }, err => {
                        loading.dismiss();
                    });
                }
                else if (this.tipo == 'emergencia') {
                    this.service.getEmergenciasDia().subscribe((data) => {
                        this.emergencias = data.data;
                        this.emergencias.filter((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                            if (data.id == this.id) {
                                this.emergencia = data;
                                this.clienteid = data.paciente_id;
                                this.trataId = data.tratamiento_id;
                                this.getPaciente(this.clienteid);
                                this.getTratamiento(this.trataId);
                                yield loading.dismiss();
                            }
                        }));
                    }, err => {
                        loading.dismiss();
                    });
                }
            }));
        });
    }
    getPaciente(id) {
        this.service.getPacientes(1, 5000).subscribe((data) => {
            this.pacientes = data.data;
            this.pacientes.filter((data) => {
                if (data.id == id) {
                    this.paciente = data;
                }
            });
        });
    }
    getTratamiento(id) {
        this.service.getTratamientos().subscribe((data) => {
            this.tratamientos = data.data;
            this.tratamientos.filter((data) => {
                if (data.id == id) {
                    this.tratamiento = data;
                }
            });
        });
    }
};
ViewModalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] }
];
ViewModalPage.propDecorators = {
    id: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    tipo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    item: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
ViewModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-view-modal',
        template: _raw_loader_view_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_view_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], ViewModalPage);



/***/ }),

/***/ "aBvg":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/view-modal/view-modal.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header *ngIf=\"tipo == 'reserva'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Reservas del día #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'reserva'\" class=\"ion-padding\">\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <div class=\"ion-text-center\">\r\n          <strong>NUM FICHA </strong>{{item.num_ficha}}\r\n\r\n      </div>\r\n      <ion-item>\r\n        <ion-avatar slot=\"start\">\r\n          <img src=\"assets/user.png\" />\r\n        </ion-avatar>\r\n        <ion-label>\r\n        <p>{{item.paciente.nombre}}</p>\r\n        <p>Tipo: {{item.tipo}}</p>\r\n        </ion-label>\r\n      </ion-item>\r\n      <br>\r\n      <ion-grid>\r\n          <ion-row>\r\n              <ion-col size=\"6\">\r\n               <b>Fecha de Reserva</b> \r\n              <p>{{item.fecha}}</p>  \r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                  <b>Tipo de tratamiento</b> \r\n  \r\n              <p>  {{item.tratamiento}}</p>\r\n  \r\n              </ion-col>\r\n            </ion-row>\r\n        <!-- <ion-row>\r\n          <ion-col class=\"ion-text-center\">\r\n            <br>\r\n            <strong>FECHA : </strong>{{reserva.fecha}}\r\n            <br>\r\n            <strong>FICHA : </strong>{{reserva.num_ficha}}\r\n            <br>\r\n            <div *ngIf=\"reserva.hora_ingreso\">\r\n              <strong>HORA DE INGRESO : </strong>{{reserva.hora_ingreso}}\r\n            </div>\r\n            <div *ngIf=\"reserva.hora_llegada\">\r\n              <strong>HORA DE LLEGADA : </strong>{{reserva.hora_llegada}}\r\n            </div>\r\n            <strong>CLIENTE : </strong>{{paciente.nombres}} {{' ' + paciente.apellidos}}\r\n            <br>\r\n            <strong>TIPO : </strong>{{reserva.tipo}}\r\n            <br>\r\n            <strong>TRATAMIENTO : </strong>{{tratamiento.nombre | titlecase}}\r\n            <br>\r\n            <strong>CANTIDAD : </strong>{{tratamiento.cantidad}} unidade(s)\r\n          </ion-col>\r\n        </ion-row> -->\r\n        <ion-row>\r\n          <ion-col class=\"ion-text-center margin\">\r\n            <ion-button (click)=\"dismiss()\">Cerrar</ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n</ion-content>\r\n\r\n<ion-header *ngIf=\"tipo == 'emergencia'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Emergencia del día #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'emergencia'\" class=\"ion-padding\">\r\n  <ion-card>\r\n    <ion-card-content>\r\n\r\n      <ion-grid>\r\n          <ion-item>\r\n              <ion-avatar slot=\"start\">\r\n                <img src=\"assets/user.png\" />\r\n              </ion-avatar>\r\n              <ion-label>\r\n              <p>{{item.paciente.nombre}}</p>\r\n              <p>Tipo: {{item.tipo}}</p>\r\n              </ion-label>\r\n            </ion-item>\r\n            <ion-row>\r\n                <ion-col size=\"6\">\r\n                 <b>Fecha de Reserva</b> \r\n                <p>{{item.fecha}}</p>  \r\n                </ion-col>\r\n                <ion-col size=\"6\">\r\n                    <b>Tipo de tratamiento</b> \r\n    \r\n                <p>  {{item.tratamiento}}</p>\r\n    \r\n                </ion-col>\r\n              </ion-row>\r\n          <!-- <ion-col class=\"ion-text-center\">\r\n            <br>\r\n            <strong>FECHA : </strong>{{emergencia.fecha}}\r\n            <br>\r\n            <strong>HORA : </strong>{{emergencia.hora}}\r\n            <br>\r\n            <div *ngIf=\"emergencia.hora_ingreso\">\r\n              <strong>HORA DE INGRESO : </strong>{{emergencia.hora_ingreso}}\r\n            </div>\r\n            <strong>CLIENTE : </strong>{{paciente.nombres}} {{' ' + paciente.apellidos}}\r\n            <br>\r\n            <strong>TIPO : </strong>{{emergencia.tipo}}\r\n            <br>\r\n            <strong>TRATAMIENTO : </strong>{{tratamiento.nombre | titlecase}}\r\n            <br>\r\n            <strong>CANTIDAD : </strong>{{tratamiento.cantidad}} unidade(s)\r\n          </ion-col> -->\r\n        <ion-row>\r\n          <ion-col class=\"ion-text-center margin\">\r\n            <ion-button (click)=\"dismiss()\">Cerrar</ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n</ion-content>");

/***/ }),

/***/ "gQTr":
/*!*************************************************!*\
  !*** ./src/app/view-modal/view-modal.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".margin {\n  margin-top: 150px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHZpZXctbW9kYWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7QUFDSiIsImZpbGUiOiJ2aWV3LW1vZGFsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYXJnaW4ge1xyXG4gICAgbWFyZ2luLXRvcDogMTUwcHg7XHJcbn0iXX0= */");

/***/ })

}]);
//# sourceMappingURL=default~tab1-tab1-module~tab2-tab2-module~view-modal-view-modal-module-es2015.js.map