(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~edit-modal-edit-modal-module~tab1-tab1-module~tab2-tab2-module"], {
    /***/
    "4Hae":
    /*!***********************************************!*\
      !*** ./src/app/edit-modal/edit-modal.page.ts ***!
      \***********************************************/

    /*! exports provided: EditModalPage */

    /***/
    function Hae(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditModalPage", function () {
        return EditModalPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_edit_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./edit-modal.page.html */
      "fVjV");
      /* harmony import */


      var _edit_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./edit-modal.page.scss */
      "wlsB");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! moment */
      "wd/R");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../services/service.service */
      "rRxC");
      /* harmony import */


      var _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ../search-page/search-page.page */
      "tw2k");
      /* harmony import */


      var _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../horas/horas.page */
      "Y2XA");

      var EditModalPage = /*#__PURE__*/function () {
        function EditModalPage(modal, loading, toast, modalController, formB, alertController, service) {
          _classCallCheck(this, EditModalPage);

          this.modal = modal;
          this.loading = loading;
          this.toast = toast;
          this.modalController = modalController;
          this.formB = formB;
          this.alertController = alertController;
          this.service = service;
          this.conditionNombreNuevo = false;
          this.conditionHora = true;
        }

        _createClass(EditModalPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log(this.item);
            this.nombres = this.item.paciente.nombres;
            this.apellidos = this.item.paciente.apellidos;
            this.idPaciente = this.item.paciente.id;
            this.dataHour = this.item.hora;
            this.fecha = this.item.fecha;

            if (this.item) {
              this.control = true;
              this.controlHora = true;
            } else {
              this.control = false;
              this.controlHora = false;
            }
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modal.dismiss();
          }
        }, {
          key: "searchCliente",
          value: function searchCliente() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal, _yield$modal$onDidDis, data;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.conditionNombreNuevo = false;
                      _context.next = 3;
                      return this.modalController.create({
                        component: _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_8__["SearchPagePage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true
                      });

                    case 3:
                      modal = _context.sent;
                      _context.next = 6;
                      return modal.present();

                    case 6:
                      _context.next = 8;
                      return modal.onDidDismiss();

                    case 8:
                      _yield$modal$onDidDis = _context.sent;
                      data = _yield$modal$onDidDis.data;
                      this.control = data.control;
                      this.nombres = data.nombres;
                      this.apellidos = data.apellidos;
                      this.idPaciente = data.id;

                    case 14:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "searchHora",
          value: function searchHora() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var modal, _yield$modal$onDidDis2, data;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      if (!this.fecha) {
                        _context2.next = 13;
                        break;
                      }

                      _context2.next = 3;
                      return this.modalController.create({
                        component: _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__["HorasPage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'fecha': this.fecha
                        }
                      });

                    case 3:
                      modal = _context2.sent;
                      _context2.next = 6;
                      return modal.present();

                    case 6:
                      _context2.next = 8;
                      return modal.onDidDismiss();

                    case 8:
                      _yield$modal$onDidDis2 = _context2.sent;
                      data = _yield$modal$onDidDis2.data;

                      if (data.dataEdit) {
                        this.controlHora = data.controlHora;
                        this.dataHour = data.data;
                      } else {
                        this.controlHora = true;
                        this.fecha = this.item.fecha;
                      }

                      _context2.next = 14;
                      break;

                    case 13:
                      this.presentAlertConfirm();

                    case 14:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "eventDate",
          value: function eventDate(e) {
            this.item.fecha = moment__WEBPACK_IMPORTED_MODULE_6__(e.detail.value).format('YYYY-MM-DD');
            this.conditionHora = true;
          }
        }, {
          key: "presentAlertConfirm",
          value: function presentAlertConfirm() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        header: 'Alerta!',
                        message: 'Se equivoco de hora tiene que seleccionar nuevamente la fecha!!!',
                        buttons: [{
                          text: 'Ok',
                          handler: function handler() {
                            _this.form.reset();
                          }
                        }]
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "edit",
          value: function edit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 2:
                      loading = _context7.sent;
                      _context7.next = 5;
                      return loading.present().then(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                          var _this3 = this;

                          return regeneratorRuntime.wrap(function _callee6$(_context6) {
                            while (1) {
                              switch (_context6.prev = _context6.next) {
                                case 0:
                                  if (this.tipo == 'reserva') {
                                    this.service.editReserva(this.item.id, this.idPaciente, moment__WEBPACK_IMPORTED_MODULE_6__(this.fecha), this.dataHour).subscribe(function (data) {
                                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                        return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                          while (1) {
                                            switch (_context4.prev = _context4.next) {
                                              case 0:
                                                this.ok('Reserva editada');
                                                loading.dismiss();
                                                this.dismiss();

                                              case 3:
                                              case "end":
                                                return _context4.stop();
                                            }
                                          }
                                        }, _callee4, this);
                                      }));
                                    }, function (err) {
                                      _this3.error('Reserva no editada');

                                      loading.dismiss();
                                    });
                                  } else if (this.tipo == 'emergencia') {
                                    this.service.editEmergencia(this.item.id, this.idPaciente, moment__WEBPACK_IMPORTED_MODULE_6__(this.fecha), this.dataHour).subscribe(function (data) {
                                      return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                                        return regeneratorRuntime.wrap(function _callee5$(_context5) {
                                          while (1) {
                                            switch (_context5.prev = _context5.next) {
                                              case 0:
                                                this.ok('Emergencia editada');
                                                this.dismiss();
                                                _context5.next = 4;
                                                return loading.dismiss();

                                              case 4:
                                              case "end":
                                                return _context5.stop();
                                            }
                                          }
                                        }, _callee5, this);
                                      }));
                                    }, function (err) {
                                      _this3.error('Emergencia no editada');

                                      loading.dismiss();
                                    });
                                  }

                                case 1:
                                case "end":
                                  return _context6.stop();
                              }
                            }
                          }, _callee6, this);
                        }));
                      });

                    case 5:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "error",
          value: function error(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var toast;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      _context8.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 3000,
                        cssClass: 'my-custom-class-alert'
                      });

                    case 2:
                      toast = _context8.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }, {
          key: "ok",
          value: function ok(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var toast;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 3000,
                        cssClass: 'my-custom-class-success'
                      });

                    case 2:
                      toast = _context9.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }]);

        return EditModalPage;
      }();

      EditModalPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_7__["ServiceService"]
        }];
      };

      EditModalPage.propDecorators = {
        item: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }],
        tipo: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }]
      };
      EditModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-edit-modal',
        template: _raw_loader_edit_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_edit_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], EditModalPage);
      /***/
    },

    /***/
    "fVjV":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edit-modal/edit-modal.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function fVjV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header *ngIf=\"tipo == 'reserva'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Editar la reserva #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'reserva'\">\r\n    <!-- <form [formGroup]=\"form\"> -->\r\n\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <!-- <ion-item>\r\n        <ion-label>Hora de ingreso</ion-label>\r\n        <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"ingreso\"></ion-datetime>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-label>Hora de llegada</ion-label>\r\n        <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"llegada\"></ion-datetime>\r\n      </ion-item> -->\r\n      <ion-item>\r\n          <ion-label>Fecha</ion-label>\r\n          <ion-input id=\"input-date\"  (ionChange)=\"eventDate($event)\" type=\"date\" [(ngModel)]=\"fecha\" min=\"{{actual\r\n          }}\" max=\"2050\" ></ion-input>\r\n        </ion-item>\r\n        <br>\r\n        <ion-item [disabled]=\"!conditionHora\" (click)=\"searchHora()\">\r\n          <ion-label >Seleccionar la hora</ion-label>\r\n        <h6 *ngIf=\"controlHora == true\">{{dataHour}}</h6>\r\n        <h6 *ngIf=\"controlHora == false\">Seleccionar la hora</h6>\r\n\r\n        </ion-item>\r\n        <br>\r\n      \r\n      <div  class=\"ion-text-center\">\r\n          <!-- <ion-item> -->\r\n            <ion-button (click)=\"searchCliente()\" expand=\"block\">\r\n              <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\r\n              <h6 *ngIf=\"control == true\">{{nombres + ' ' + apellidos}}</h6>\r\n              <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\r\n            </ion-button>\r\n            <br>\r\n        </div>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-button (click)=\"edit()\" class=\"margin\" expand=\"block\">\r\n    Editar la reserva\r\n    <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\r\n  </ion-button>\r\n<!-- </form> -->\r\n</ion-content>\r\n\r\n<ion-header *ngIf=\"tipo == 'emergencia'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Editar la emergencia #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'emergencia'\">\r\n  <ion-card>\r\n    <ion-card-content>\r\n        <ion-item>\r\n            <ion-label>Fecha</ion-label>\r\n            <ion-input id=\"input-date\"   type=\"date\" [(ngModel)]=\"fecha\" min=\"{{actual\r\n            }}\" max=\"2050\" ></ion-input>\r\n          </ion-item>\r\n      <ion-item>\r\n        <ion-label>Hora de ingreso</ion-label>\r\n        <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"dataHour\"></ion-datetime>\r\n      </ion-item>\r\n         \r\n      <div  class=\"ion-text-center\">\r\n          <!-- <ion-item> -->\r\n            <ion-button (click)=\"searchCliente()\" expand=\"block\">\r\n              <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\r\n              <h6 *ngIf=\"control == true\">{{nombres + ' ' + apellidos}}</h6>\r\n              <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\r\n            </ion-button>\r\n            <br>\r\n        </div>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-button (click)=\"edit()\" class=\"margin\" expand=\"block\">\r\n    Editar la emergencia\r\n    <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\r\n  </ion-button>\r\n\r\n</ion-content>";
      /***/
    },

    /***/
    "wlsB":
    /*!*************************************************!*\
      !*** ./src/app/edit-modal/edit-modal.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function wlsB(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlZGl0LW1vZGFsLnBhZ2Uuc2NzcyJ9 */";
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~edit-modal-edit-modal-module~tab1-tab1-module~tab2-tab2-module-es5.js.map