(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"],{

/***/ "8MT7":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <!-- <br> -->\r\n    <ion-title class=\"ion-text-center\">Reservas del día</ion-title>\r\n    <!-- <ion-button (click)=\"createReserva()\" class=\"margin\" expand=\"block\" color=\"success\">\r\n        Crear Reserva\r\n        <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\r\n      </ion-button> -->\r\n      <ion-buttons slot=\"end\">\r\n          <ion-button (click)=\"createReserva()\" color=\"light\">\r\n            <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"add\"></ion-icon>\r\n          </ion-button>\r\n        </ion-buttons>\r\n        \r\n  </ion-toolbar>\r\n  <ion-grid fixed>\r\n   \r\n  <ion-row>\r\n      <ion-col  class=\"ion-text-center\"  size=\"6\">\r\n        <br>\r\n        <!-- <ion-item>\r\n          <ion-label position=\"floating\">Fecha de Reservas</ion-label>\r\n        <ion-input id=\"input-date\" type=\"date\" [(ngModel)]=\"fecha\" ></ion-input>\r\n      </ion-item> -->\r\n        \r\n        <ion-label class=\"label-date\" >Fecha de Reservas</ion-label>\r\n        <!-- <ion-input id=\"input-date\" class=\"custom-class\" type=\"date\" [(ngModel)]=\"fecha\" placeholder=\"DD/MM/YYYY\"></ion-input> -->\r\n        <input min=\"{{actual}}\" type=\"date\"  [(ngModel)]=\"fecha\" />\r\n        <!-- <input id=\"input-date\" [(ngModel)]=\"fecha\" type=\"date\" placeholder=\"DD/MM/YYYY\"></input> -->\r\n          <!-- <ion-datetime [(ngModel)]=\"fecha\" display-format=\"DD-MM-YYYY\" placeholder=\"DD/MM/YYYY\"></ion-datetime> -->\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n          <ion-button (click)=\"searchFilterReserva()\" expand=\"block\"  shape=\"round\">\r\n              Buscar\r\n            </ion-button>\r\n            <p *ngIf=\"buttonOn\" >\r\n            <ion-button (click)=\"reset()\" expand=\"block\" shape=\"round\">\r\n                Limpiar\r\n              </ion-button>\r\n            </p>\r\n      </ion-col>\r\n    </ion-row></ion-grid>\r\n</ion-header>\r\n\r\n<ion-content >\r\n\r\n<!-- <div>\r\n<br> -->\r\n  <!-- <ion-grid fixed>\r\n    <ion-row>\r\n      <ion-col  class=\"ion-text-center\"  size=\"6\">\r\n        <ion-label position=\"inline\">Fecha de Reservas</ion-label>\r\n          <ion-datetime [(ngModel)]=\"fecha\" display-format=\"DD-MM-YYYY\" placeholder=\"DD/MM/YYYY\"></ion-datetime>\r\n      </ion-col>\r\n      <ion-col size=\"6\">\r\n          <ion-button (click)=\"searchFilterReserva()\" expand=\"block\"  shape=\"round\">\r\n              Buscar\r\n            </ion-button>\r\n            <p *ngIf=\"buttonOn\" >\r\n            <ion-button (click)=\"reset()\" expand=\"block\" shape=\"round\">\r\n                Limpiar\r\n              </ion-button>\r\n            </p>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid> -->\r\n\r\n  \r\n\r\n  <ion-list *ngIf=\"reservas.length == 0 && !det\">\r\n    <ion-button expand=\"block\">\r\n      Cargando\r\n      <ion-icon name=\"reload\" style=\"font-size: 25px;\" slot=\"end\" #loadingIcon></ion-icon>\r\n    </ion-button>\r\n  </ion-list>\r\n\r\n  <ion-list *ngIf=\"reservas.length == 0 && det\">\r\n    <ion-item>\r\n      <span class=\"text danger\">No hay reservas del día</span>\r\n    </ion-item>\r\n  </ion-list>\r\n\r\n\r\n  <ion-list *ngIf=\"reservas.length >= 1 && det\">\r\n    <ion-item-sliding *ngFor=\"let item of reservas\" >\r\n        <ion-item>\r\n          <ion-row>\r\n              <ion-col size=\"12\">\r\n                  <ion-label>\r\n                      <p id=\"name\"><b>{{item.paciente.nombre}}</b> </p>\r\n                      <p id=\"name\" *ngIf=\"item.observacion\" ><b>{{item.observacion}}</b> </p>\r\n                      <ion-row>\r\n                        <ion-col size=\"6\">\r\n                            <p>{{item.hora}}</p>\r\n                        </ion-col>\r\n                        <ion-col class=\"ion-text-left\" size=\"6\">\r\n                            <p >Tipo: <span>{{item.tipo}}</span> </p> \r\n                          </ion-col>\r\n                      </ion-row>\r\n                  </ion-label>\r\n                </ion-col>\r\n          </ion-row>\r\n        <ion-note slot=\"end\">\r\n          Deslizar\r\n        </ion-note>\r\n      </ion-item>\r\n      <ion-item-options side=\"end\">\r\n        <ion-item-option (click)=\"viewReserva(item.id,item)\">\r\n          <ion-icon id=\"add\"  slot=\"icon-only\" name=\"eye\"></ion-icon>\r\n        </ion-item-option>\r\n        <ion-item-option (click)=\"editReserva(item)\">\r\n          <ion-icon id=\"edit-menu\"  slot=\"icon-only\" name=\"create\"></ion-icon>\r\n        </ion-item-option>\r\n        <ion-item-option (click)=\"deleteReserva(item.id)\">\r\n          <ion-icon id=\"eliminar\"  slot=\"icon-only\" name=\"trash\"></ion-icon>\r\n        </ion-item-option>\r\n      </ion-item-options>\r\n  \r\n    </ion-item-sliding>\r\n  </ion-list>\r\n\r\n</ion-content>");

/***/ }),

/***/ "Mzl2":
/*!***********************************!*\
  !*** ./src/app/tab1/tab1.page.ts ***!
  \***********************************/
/*! exports provided: Tab1Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1Page", function() { return Tab1Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./tab1.page.html */ "8MT7");
/* harmony import */ var _tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tab1.page.scss */ "rWyk");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");
/* harmony import */ var _view_modal_view_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../view-modal/view-modal.page */ "9Tgf");
/* harmony import */ var _edit_modal_edit_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../edit-modal/edit-modal.page */ "4Hae");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _create_page_create_page_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../create-page/create-page.page */ "LXxN");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);











let Tab1Page = class Tab1Page {
    // type:any;
    constructor(service, animationCtrl, loading, modalController, alert, toast, router) {
        this.service = service;
        this.animationCtrl = animationCtrl;
        this.loading = loading;
        this.modalController = modalController;
        this.alert = alert;
        this.toast = toast;
        this.router = router;
        this.actual = moment__WEBPACK_IMPORTED_MODULE_10__().format('YYYY-MM-DD');
        this.getReservas();
    }
    ionViewDidEnter() {
        this.getReservas();
        this.fecha = '';
    }
    getReservas() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.fecha = '';
            this.buttonOn = true;
            this.det = '';
            this.reservas = [];
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            this.animate();
            yield loading.present().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.service.getReservasDia().subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.reservas = data.data;
                    this.det = 'lleno';
                    loading.dismiss();
                }), err => {
                    loading.dismiss();
                });
            }));
        });
    }
    searchFilterReserva() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let fechaFilter = moment__WEBPACK_IMPORTED_MODULE_10__(this.fecha).format('YYYY-MM-DD');
            this.det = '';
            this.reservas = [];
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            this.animate();
            yield loading.present().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                this.service.searchReserva(fechaFilter).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    this.reservas = data.data;
                    this.det = 'lleno';
                    loading.dismiss();
                }), err => {
                    loading.dismiss();
                });
            }));
        });
    }
    reset() {
        this.buttonOn = false;
        this.fecha = '';
        this.getReservas();
    }
    animate() {
        const loadingAnimation = this.animationCtrl.create('loading-animation')
            .addElement(this.loadingIcon.nativeElement)
            .duration(1500)
            .iterations(3)
            .fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
        loadingAnimation.play();
    }
    viewReserva(id, item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _view_modal_view_modal_page__WEBPACK_IMPORTED_MODULE_6__["ViewModalPage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
                componentProps: {
                    'id': id,
                    'tipo': 'reserva',
                    'item': item
                }
            });
            return yield modal.present();
        });
    }
    editReserva(item) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _edit_modal_edit_modal_page__WEBPACK_IMPORTED_MODULE_7__["EditModalPage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
                componentProps: {
                    'item': item,
                    'tipo': 'reserva'
                }
            });
            modal.onDidDismiss().then(() => {
                this.getReservas();
            });
            return yield modal.present();
        });
    }
    deleteReserva(id) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alert.create({
                cssClass: 'my-custom-class',
                header: 'Confirm!',
                message: '¿Eliminar Reserva?',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        cssClass: 'secondary',
                        handler: (blah) => {
                        }
                    }, {
                        text: 'Okay',
                        handler: () => {
                            this.service.deleteReserva(id).subscribe((data) => {
                                this.ok('Reserva eliminada');
                                this.getReservas();
                            }, err => {
                                this.error(err);
                            });
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    createReserva() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _create_page_create_page_page__WEBPACK_IMPORTED_MODULE_9__["CreatePagePage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
                componentProps: {
                    'tipo': 'reserva'
                }
            });
            modal.onDidDismiss().then(() => {
                this.getReservas();
            });
            return yield modal.present();
        });
    }
    ok(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-success'
            });
            toast.present();
        });
    }
    error(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-alert'
            });
            toast.present();
        });
    }
};
Tab1Page.ctorParameters = () => [
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AnimationController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"] }
];
Tab1Page.propDecorators = {
    loadingIcon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: ['loadingIcon', { read: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"] },] }]
};
Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab1',
        template: _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], Tab1Page);



/***/ }),

/***/ "XOzS":
/*!*********************************************!*\
  !*** ./src/app/tab1/tab1-routing.module.ts ***!
  \*********************************************/
/*! exports provided: Tab1PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function() { return Tab1PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tab1.page */ "Mzl2");




const routes = [
    {
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"],
    }
];
let Tab1PageRoutingModule = class Tab1PageRoutingModule {
};
Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Tab1PageRoutingModule);



/***/ }),

/***/ "rWyk":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n\nion-item-option {\n  background: white;\n}\n\n.label-date {\n  color: black;\n}\n\nimg {\n  border-radius: 50px;\n  width: 50px;\n  height: 50px;\n}\n\n#name {\n  font-size: 0.7em;\n  color: black;\n}\n\nion-row {\n  width: 100%;\n}\n\ninput[type=date]:before {\n  content: attr(placeholder) !important;\n  color: #aaa;\n  margin-right: 0.5em;\n}\n\ninput[type=date]:focus:before,\ninput[type=date]:valid:before {\n  content: \"\";\n}\n\nion-datetime {\n  color: black;\n}\n\n.date-placeholder {\n  display: inline-block;\n  position: absolute;\n  text-align: left;\n  color: #aaa;\n  background-color: white;\n  cursor: text;\n  /* Customize this stuff based on your styles */\n  top: 4px;\n  left: 4px;\n  right: 4px;\n  bottom: 4px;\n  line-height: 32px;\n  padding-left: 12px;\n}\n\nion-grid {\n  background: white;\n}\n\n#add {\n  color: #27ae60;\n}\n\n#edit-menu {\n  color: #2980b9;\n}\n\n#eliminar {\n  color: #c0392b;\n}\n\nion-list {\n  padding-top: 30px;\n}\n\n.text {\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.danger {\n  color: red;\n  font-weight: bolder;\n}\n\n.margin {\n  margin-top: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHRhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxpQkFBQTtBQUVKOztBQUFBO0VBQ0ksWUFBQTtBQUdKOztBQURBO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUlKOztBQUZBO0VBQ0ksZ0JBQUE7RUFDQSxZQUFBO0FBS0o7O0FBSEE7RUFDSSxXQUFBO0FBTUo7O0FBQ0E7RUFDSSxxQ0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQUVKOztBQUFFOztFQUVFLFdBQUE7QUFHSjs7QUFDQTtFQUNJLFlBQUE7QUFFSjs7QUFBQTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSw4Q0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBR0o7O0FBQUE7RUFDSSxpQkFBQTtBQUdKOztBQURBO0VBQ0ksY0FBQTtBQUlKOztBQUZBO0VBQ0ksY0FBQTtBQUtKOztBQUhBO0VBQ0ksY0FBQTtBQU1KOztBQUpBO0VBQ0ksaUJBQUE7QUFPSjs7QUFMQTtFQUNJLHFCQUFBO0VBQ0Esc0JBQUE7QUFRSjs7QUFOQTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtBQVNKOztBQU5BO0VBQ0ksZ0JBQUE7QUFTSiIsImZpbGUiOiJ0YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b25zIHtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcbmlvbi1pdGVtLW9wdGlvbntcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG59XHJcbi5sYWJlbC1kYXRle1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbmltZ3tcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICB3aWR0aDogNTBweDtcclxuICAgIGhlaWdodDogNTBweDtcclxufVxyXG4jbmFtZXtcclxuICAgIGZvbnQtc2l6ZTogMC43ZW07XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbn1cclxuaW9uLXJvd3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcbi8vICNpbnB1dC1kYXRle1xyXG4vLyAgICAgLS1jb2xvcjogd2hpdGU7XHJcbi8vICAgICAtLWJhY2tncm91bmQ6ICMzODgwZmY7XHJcbi8vICAgICAvLyBib3JkZXI6IG5vbmU7XHJcbi8vIH1cclxuaW5wdXRbdHlwZT1cImRhdGVcIl06YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IGF0dHIocGxhY2Vob2xkZXIpICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogI2FhYTtcclxuICAgIG1hcmdpbi1yaWdodDogMC41ZW07XHJcbiAgfVxyXG4gIGlucHV0W3R5cGU9XCJkYXRlXCJdOmZvY3VzOmJlZm9yZSxcclxuICBpbnB1dFt0eXBlPVwiZGF0ZVwiXTp2YWxpZDpiZWZvcmUge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICB9XHJcblxyXG4gIFxyXG5pb24tZGF0ZXRpbWV7XHJcbiAgICBjb2xvcjogYmxhY2s7IFxyXG59XHJcbi5kYXRlLXBsYWNlaG9sZGVyIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBjb2xvcjogI2FhYTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgY3Vyc29yOiB0ZXh0O1xyXG4gICAgLyogQ3VzdG9taXplIHRoaXMgc3R1ZmYgYmFzZWQgb24geW91ciBzdHlsZXMgKi9cclxuICAgIHRvcDogNHB4O1xyXG4gICAgbGVmdDogNHB4O1xyXG4gICAgcmlnaHQ6IDRweDtcclxuICAgIGJvdHRvbTogNHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDMycHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XHJcbn1cclxuXHJcbmlvbi1ncmlke1xyXG4gICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbn1cclxuI2FkZCB7XHJcbiAgICBjb2xvcjogIzI3YWU2MDtcclxufVxyXG4jZWRpdC1tZW51IHtcclxuICAgIGNvbG9yOiAjMjk4MGI5O1xyXG59XHJcbiNlbGltaW5hciB7XHJcbiAgICBjb2xvcjogI2MwMzkyYjtcclxufVxyXG5pb24tbGlzdCB7XHJcbiAgICBwYWRkaW5nLXRvcDogMzBweDtcclxufVxyXG4udGV4dCB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG59XHJcbi5kYW5nZXIge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XHJcbn1cclxuXHJcbi5tYXJnaW4ge1xyXG4gICAgbWFyZ2luLXRvcDogMjVweDtcclxufVxyXG4iXX0= */");

/***/ }),

/***/ "tmrb":
/*!*************************************!*\
  !*** ./src/app/tab1/tab1.module.ts ***!
  \*************************************/
/*! exports provided: Tab1PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function() { return Tab1PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./tab1.page */ "Mzl2");
/* harmony import */ var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../explore-container/explore-container.module */ "qtYk");
/* harmony import */ var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./tab1-routing.module */ "XOzS");








let Tab1PageModule = class Tab1PageModule {
};
Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"],
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"],
            _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"]
        ],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
    })
], Tab1PageModule);



/***/ })

}]);
//# sourceMappingURL=tab1-tab1-module-es2015.js.map