(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~create-page-create-page-module~edit-modal-edit-modal-module~search-page-search-page-module~t~b5c43584"], {
    /***/
    "AVkp":
    /*!***************************************************!*\
      !*** ./src/app/search-page/search-page.page.scss ***!
      \***************************************************/

    /*! exports provided: default */

    /***/
    function AVkp(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "#add {\n  color: #27ae60;\n}\n\n.buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXHNlYXJjaC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNlYXJjaC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhZGQge1xyXG4gICAgY29sb3I6ICMyN2FlNjA7XHJcbn1cclxuXHJcbi5idXR0b25zIHtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcbiJdfQ== */";
      /***/
    },

    /***/
    "kUx7":
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search-page/search-page.page.html ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function kUx7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>Buscar cliente</ion-title>\r\n \r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-searchbar placeholder=\"Buscar...\" [(ngModel)]=\"filterTerm\" animated=\"true\" [debounce]=\"2000\"\r\n(ionChange)=\"searchProductos()\"\r\n[autocomplete]=\"true\"\r\n\r\n></ion-searchbar>\r\n<ion-content>\r\n\r\n\r\n  <ion-list>\r\n    <ion-item *ngFor=\"let data of itemListData \" (click)=\"select(data)\">\r\n      <ion-label>{{( data.nombres | titlecase) + ' ' + (data.apellidos | titlecase)}}</ion-label>\r\n      <ion-icon id=\"add\" (click)=\"select(data)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\r\n    </ion-item>\r\n    <!-- <ion-virtual-scroll [items]=\"pacientes\" approxItemHeight=\"320px\">\r\n        <ion-card *virtualItem=\"let item;\">\r\n            <ion-item (click)=\"select(item)\">\r\n                <ion-label>{{(item.apellidos | titlecase) + ' ' + (item.nombres | titlecase)}}</ion-label>\r\n                <ion-icon id=\"add\" (click)=\"select(item)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\r\n              </ion-item>\r\n        </ion-card>\r\n      </ion-virtual-scroll> -->\r\n    \r\n  </ion-list>\r\n  <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\r\n      <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\r\n      </ion-infinite-scroll-content>\r\n    </ion-infinite-scroll>\r\n</ion-content>";
      /***/
    },

    /***/
    "rRxC":
    /*!*********************************************!*\
      !*** ./src/app/services/service.service.ts ***!
      \*********************************************/

    /*! exports provided: ServiceService */

    /***/
    function rRxC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ServiceService", function () {
        return ServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var ServiceService = /*#__PURE__*/function () {
        function ServiceService(http) {
          _classCallCheck(this, ServiceService);

          this.http = http;
          this.path = "https://c2100160.ferozo.com/api/user/";
        }

        _createClass(ServiceService, [{
          key: "getPacientes",
          value: function getPacientes(page, page_size) {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + 'pacientes?page=' + page + '&page_size=' + page_size + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getTratamientos",
          value: function getTratamientos() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "tratamientos", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getReservasDia",
          value: function getReservasDia() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "reservas", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "createReserva",
          value: function createReserva() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
            var fecha = arguments.length > 1 ? arguments[1] : undefined;
            var hora = arguments.length > 2 ? arguments[2] : undefined;
            var tipo = arguments.length > 3 ? arguments[3] : undefined;
            var ficha = arguments.length > 4 ? arguments[4] : undefined;
            var observacion = arguments.length > 5 ? arguments[5] : undefined;
            var tratamiento = arguments.length > 6 ? arguments[6] : undefined;
            var datoaEnviar = {
              "paciente_id": id,
              "fecha": fecha,
              "hora": hora,
              "tipo": tipo,
              // "num_ficha": ficha,
              "observacion": observacion,
              "tratamiento_id": tratamiento
            };
            return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "editReserva",
          value: function editReserva(id, paciente_id, fecha, hora) {
            var datoaEnviar = {
              "reserva_id": id,
              // "hora_ingreso": hora_ingreso,
              // "hora_llegada": hora_llegada
              "paciente_id": paciente_id,
              "hora": hora,
              "fecha": fecha
            };
            return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "deleteReserva",
          value: function deleteReserva(id) {
            var datoaEnviar = {
              "reserva_id": id
            };
            return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getEmergenciasDia",
          value: function getEmergenciasDia() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "emergencias", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "createEmergencia",
          value: function createEmergencia() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            var fecha = arguments.length > 1 ? arguments[1] : undefined;
            var hora = arguments.length > 2 ? arguments[2] : undefined;
            var observacion = arguments.length > 3 ? arguments[3] : undefined;
            var tratamiento = arguments.length > 4 ? arguments[4] : undefined;
            var datoaEnviar = {
              "paciente_id": id,
              "fecha": fecha,
              "hora": hora,
              // "tipo": tipo,
              "observacion": observacion,
              "tratamiento_id": tratamiento
            };
            return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "editEmergencia",
          value: function editEmergencia(id, paciente_id, fecha, hora) {
            var datoaEnviar = {
              "emergencia_id": id,
              "paciente_id": paciente_id,
              "hora": hora,
              "fecha": fecha
            };
            return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "deleteEmergencia",
          value: function deleteEmergencia(id) {
            var datoaEnviar = {
              "emergencia_id": id
            };
            return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchProductos",
          value: function searchProductos(nombre) {
            var datoaEnviar = {
              "nombre": nombre
            };
            return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchReserva",
          value: function searchReserva(fecha) {
            var datoaEnviar = {
              "fecha": fecha
            };
            return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchReservaTratamiento",
          value: function searchReservaTratamiento(fecha) {
            var datoaEnviar = {
              "fecha": fecha
            };
            return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }]);

        return ServiceService;
      }();

      ServiceService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      ServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ServiceService);
      /***/
    },

    /***/
    "tw2k":
    /*!*************************************************!*\
      !*** ./src/app/search-page/search-page.page.ts ***!
      \*************************************************/

    /*! exports provided: SearchPagePage */

    /***/
    function tw2k(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SearchPagePage", function () {
        return SearchPagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./search-page.page.html */
      "kUx7");
      /* harmony import */


      var _search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./search-page.page.scss */
      "AVkp");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../services/service.service */
      "rRxC");

      var SearchPagePage = /*#__PURE__*/function () {
        function SearchPagePage(service, loading, modal) {
          _classCallCheck(this, SearchPagePage);

          this.service = service;
          this.loading = loading;
          this.modal = modal;
          this.itemListData = [];
          this.page_number = 1;
          this.page_limit = 100;
        }

        _createClass(SearchPagePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getPacientes(false, "");
          }
        }, {
          key: "getPacientes",
          value: function getPacientes(isFirstLoad, event) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this = this;

              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      // const loading = await this.loading.create({
                      //   spinner: 'bubbles',
                      //   translucent: true,
                      // });
                      // await loading.present().then(async () => {
                      this.service.getPacientes(this.page_number, this.page_limit).subscribe(function (data) {
                        data = data.data;

                        for (var i = 0; i < data.length; i++) {
                          _this.itemListData.push(data[i]);
                        }

                        if (isFirstLoad) event.target.complete();
                        _this.page_number++; // loading.present();
                      }, function (error) {//  loading.present();
                      }); // })

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "doInfinite",
          value: function doInfinite(event) {
            this.getPacientes(true, event);
          }
        }, {
          key: "select",
          value: function select(data) {
            this.modal.dismiss({
              id: data.id,
              nombres: data.nombres,
              apellidos: data.apellidos,
              control: true
            });
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modal.dismiss({
              id: '',
              nombres: '',
              apellidos: '',
              control: false
            });
          }
        }, {
          key: "searchProductos",
          value: function searchProductos() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this2 = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (!(this.filterTerm === '')) {
                        _context3.next = 5;
                        break;
                      }

                      this.page_number = 1;
                      this.getPacientes(false, "");
                      _context3.next = 11;
                      break;

                    case 5:
                      this.itemListData = [];
                      _context3.next = 8;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 8:
                      loading = _context3.sent;
                      _context3.next = 11;
                      return loading.present().then(function () {
                        _this2.service.searchProductos(_this2.filterTerm).subscribe(function (data) {
                          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                            return regeneratorRuntime.wrap(function _callee2$(_context2) {
                              while (1) {
                                switch (_context2.prev = _context2.next) {
                                  case 0:
                                    this.itemListData = data.data;
                                    _context2.next = 3;
                                    return loading.dismiss();

                                  case 3:
                                  case "end":
                                    return _context2.stop();
                                }
                              }
                            }, _callee2, this);
                          }));
                        }, function (err) {
                          loading.dismiss();
                        });
                      });

                    case 11:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return SearchPagePage;
      }();

      SearchPagePage.ctorParameters = function () {
        return [{
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }];
      };

      SearchPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-search-page',
        template: _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], SearchPagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~create-page-create-page-module~edit-modal-edit-modal-module~search-page-search-page-module~t~b5c43584-es5.js.map