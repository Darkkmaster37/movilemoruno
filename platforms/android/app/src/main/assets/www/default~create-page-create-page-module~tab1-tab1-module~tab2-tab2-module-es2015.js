(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~create-page-create-page-module~tab1-tab1-module~tab2-tab2-module"],{

/***/ "LXxN":
/*!*************************************************!*\
  !*** ./src/app/create-page/create-page.page.ts ***!
  \*************************************************/
/*! exports provided: CreatePagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePagePage", function() { return CreatePagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_create_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./create-page.page.html */ "WaYu");
/* harmony import */ var _create_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./create-page.page.scss */ "iZK/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../search-page/search-page.page */ "tw2k");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../horas/horas.page */ "Y2XA");










let CreatePagePage = class CreatePagePage {
    constructor(formB, service, modalController, loading, toast, modal, alertController) {
        this.formB = formB;
        this.service = service;
        this.modalController = modalController;
        this.loading = loading;
        this.toast = toast;
        this.modal = modal;
        this.alertController = alertController;
        this.hora = [];
        this.horas = '00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23';
        this.horasTime = ['10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00',
            '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00',
            '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00',
            '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00'];
        this.conditionNombreNuevo = false;
        this.conditionHora = false;
    }
    ngOnInit() {
        if (this.tipo == 'reserva') {
            this.form = this.formB.group({
                fecha: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                // hora: ['', Validators.required],
                tipo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            });
        }
        else if (this.tipo == 'emergencia') {
            this.form = this.formB.group({
                fecha: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                hora: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            });
        }
        this.getTratamientos();
        // this.getHora();
        this.control = false;
        this.actual = moment__WEBPACK_IMPORTED_MODULE_8__().format('YYYY-MM-DD');
        this.actualHora = moment__WEBPACK_IMPORTED_MODULE_8__().format('HH:mm');
    }
    searchCliente() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.conditionNombreNuevo = false;
            const modal = yield this.modalController.create({
                component: _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_7__["SearchPagePage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
            });
            yield modal.present();
            const { data } = yield modal.onDidDismiss();
            this.control = data.control;
            this.nombres = data.nombres;
            this.apellidos = data.apellidos;
            this.id = data.id;
        });
    }
    searchHora() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.hourRecover = this.fechaLoad;
            // if (this.fechaLoad) {
            const modal = yield this.modalController.create({
                component: _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__["HorasPage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
                componentProps: {
                    'fecha': this.hourRecover
                }
            });
            yield modal.present();
            const { data } = yield modal.onDidDismiss();
            this.controlHora = data.controlHora;
            // this.nombres = data.nombres;
            // this.apellidos = data.apellidos;
            this.dataHour = data.data;
            this.fechaLoad = data.dateFechaReload;
            console.log(this.fechaLoad);
            // } else {
            // this.presentAlertConfirm();
            // }
        });
    }
    presentAlertConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Alerta!',
                message: 'Se equivoco de hora tiene que seleccionar nuevamente la fecha!!!',
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.form.reset();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    eventDate(e) {
        this.fechaLoad = moment__WEBPACK_IMPORTED_MODULE_8__(e.detail.value).format('YYYY-MM-DD');
        this.conditionHora = true;
    }
    getTratamientos() {
        this.service.getTratamientos().subscribe((data) => {
            this.tratamientos = data.data;
        });
    }
    createReserva() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            yield loading.present().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                if (this.tipo == 'reserva') {
                    if (this.id == '') {
                        this.id = null;
                    }
                    this.service.createReserva(this.id, moment__WEBPACK_IMPORTED_MODULE_8__(this.form.value.fecha).format('YYYY-MM-DD'), this.dataHour, this.form.value.tipo, this.form, this.observacion, this.tratamiento).subscribe((data) => {
                        this.ok('Reserva creada');
                        loading.dismiss();
                        this.dismiss();
                    }, err => {
                        loading.dismiss();
                        this.error('No se registro la reserva');
                    });
                }
                else if (this.tipo == 'emergencia') {
                    this.service.createEmergencia(this.id, moment__WEBPACK_IMPORTED_MODULE_8__(this.form.value.fecha).format('YYYY-MM-DD'), moment__WEBPACK_IMPORTED_MODULE_8__(this.form.value.hora).format('HH:mm'), this.observacion, this.tratamiento_emergencia).subscribe((data) => {
                        this.ok('Emergencia creada');
                        loading.dismiss();
                        this.dismiss();
                    }, err => {
                        loading.dismiss();
                        this.error('No se registro la emergencia');
                    });
                }
            }));
        });
    }
    dismiss() {
        this.modal.dismiss();
    }
    ok(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-success'
            });
            toast.present();
        });
    }
    error(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-alert'
            });
            toast.present();
        });
    }
    getHora() {
        this.service.searchReserva(this.dataHour).subscribe((data) => {
            for (let index = 0; index < data.data.length; index++) {
                let hor = data.data[index].hora;
                this.hora.push(hor);
            }
            for (let index = 0; index < this.hora.length; index++) {
                this.horasTime.splice(this.horasTime.indexOf(this.hora[index]), 1);
            }
        });
    }
    openText() {
        this.id = null;
        this.control = false;
        this.conditionNombreNuevo = true;
    }
};
CreatePagePage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] }
];
CreatePagePage.propDecorators = {
    tipo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
CreatePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-create-page',
        template: _raw_loader_create_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_create_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CreatePagePage);



/***/ }),

/***/ "WaYu":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/create-page/create-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header *ngIf=\"tipo == 'reserva'\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-center\">Crear reserva</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'reserva'\">\r\n  <form [formGroup]=\"form\">\r\n    <ion-card>\r\n      <ion-card-content>\r\n     \r\n        <ion-item>\r\n          <ion-label>Fecha</ion-label>\r\n          <ion-input id=\"input-date\"  (ionChange)=\"eventDate($event)\" type=\"date\" formControlName=\"fecha\" min=\"{{actual\r\n          }}\" max=\"2050\" ></ion-input>\r\n        </ion-item>\r\n        <br>\r\n        <ion-item [disabled]=\"!conditionHora\" (click)=\"searchHora()\">\r\n          <ion-label >Seleccionar la hora</ion-label>\r\n        <h6 *ngIf=\"controlHora == true\">{{dataHour}}</h6>\r\n        <h6 *ngIf=\"controlHora == false\">Seleccionar la hora</h6>\r\n\r\n        </ion-item>\r\n        <br>\r\n        <div  class=\"ion-text-center\">\r\n          <!-- <ion-item> -->\r\n            <ion-button (click)=\"searchCliente()\" expand=\"block\">\r\n              <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\r\n              <h6 *ngIf=\"control == true\">{{nombres + ' ' + apellidos}}</h6>\r\n              <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\r\n            </ion-button>\r\n            <br>\r\n            <ion-button (click)=\"openText()\" expand=\"block\">\r\n                <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>Nuevo cliente\r\n              </ion-button>\r\n          <!-- </ion-item> -->\r\n          <ion-item *ngIf=\"conditionNombreNuevo\" >\r\n              <ion-label>Nombre de Cliente</ion-label>\r\n             <ion-input [(ngModel)]=\"observacion\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" ></ion-input>\r\n            </ion-item>\r\n        </div>\r\n        <br>\r\n        <ion-item>\r\n          <ion-label>Tipo</ion-label>\r\n          <ion-select formControlName=\"tipo\" interface=\"action-sheet\" cancelText=\"Cancelar\" placeholder=\"Seleccionar\">\r\n            <ion-select-option value=\"Consulta\">Consulta</ion-select-option>\r\n            <ion-select-option value=\"Reconsulta\">Reconsulta</ion-select-option>\r\n            <ion-select-option value=\"Teleconsulta\">Teleconsulta</ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n        <!-- <ion-item>\r\n          <ion-label>Ficha</ion-label>\r\n          <ion-input type=\"number\" placeholder=\"Número de ficha\" formControlName=\"numFicha\">\r\n          </ion-input>\r\n        </ion-item> -->\r\n        <br>\r\n        <ion-item>\r\n          <ion-label>Tratamiento</ion-label>\r\n          <ion-select [(ngModel)]=\"tratamiento\" [ngModelOptions]=\"{standalone: true}\" interface=\"action-sheet\" cancelText=\"Cancelar\"\r\n            placeholder=\"Seleccionar\">\r\n            <ion-select-option *ngFor=\"let item of tratamientos\" value=\"{{item.id}}\">{{item.nombre | titlecase}}\r\n            </ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n        <div class=\"ion-text-center margin\">\r\n          <ion-button [disabled]=\"!form.valid  || controlHora == false\" (click)=\"createReserva()\" type=\"submit\" shape=\"round\"\r\n            id=\"principal-color-button\">\r\n            Registrar\r\n          </ion-button>\r\n        </div>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </form>\r\n</ion-content>\r\n\r\n<ion-header *ngIf=\"tipo == 'emergencia'\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title class=\"ion-text-center\">Crear tratamiento</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'emergencia'\">\r\n  <form [formGroup]=\"form\">\r\n    <ion-card>\r\n      <ion-card-content>\r\n        <!-- <ion-item > -->\r\n          <div class=\"ion-text-center\">\r\n              <ion-button (click)=\"searchCliente()\" expand=\"block\">\r\n                  <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\r\n                  <h6 *ngIf=\"control == true\">{{nombres + ' ' + apellidos }}</h6>\r\n                  <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\r\n                </ion-button>\r\n                <br>\r\n                <ion-button (click)=\"openText()\" expand=\"block\">\r\n                    <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>Nuevo cliente\r\n                  </ion-button>\r\n                <ion-item *ngIf=\"conditionNombreNuevo\" >\r\n                  <ion-label>Nombre de Cliente</ion-label>\r\n                 <ion-input [(ngModel)]=\"observacion\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" ></ion-input>\r\n                </ion-item>\r\n          </div>\r\n          \r\n        <!-- </ion-item> -->\r\n        <br>\r\n        <ion-item>\r\n          <ion-label>Fecha</ion-label>\r\n          <!-- <ion-datetime formControlName=\"fecha\" interface=\"action-sheet\" min=\"{{actual\r\n          }}\" max=\"2050\" value=\"{{actual}}\" display-format=\"YYYY-MMM-DD\" placeholder=\"Seleccionar\"\r\n            monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\">\r\n          </ion-datetime> -->\r\n          <ion-input id=\"input-date\" min=\"{{actual\r\n          }}\" max=\"2050\" type=\"date\" formControlName=\"fecha\" ></ion-input>\r\n        </ion-item>\r\n        <br>\r\n        <ion-item>\r\n            <ion-label>Seleccionar la hora</ion-label>\r\n            <ion-datetime minuteValues=\"00,15,30,45\" formControlName=\"hora\" interface=\"action-sheet\"\r\n              placeholder=\"Seleccionar\" display-format=\"HH:mm\"\r\n              picker-format=\"HH:mm\">\r\n            </ion-datetime>\r\n          </ion-item>\r\n          <br>\r\n        <!-- <ion-item>\r\n          <ion-label>Tipo</ion-label>\r\n          <ion-select formControlName=\"tipo\" interface=\"action-sheet\" cancelText=\"Cancelar\" placeholder=\"Seleccionar\">\r\n            <ion-select-option value=\"Consulta\">Consulta</ion-select-option>\r\n            <ion-select-option value=\"Reconsulta\">Reconsulta</ion-select-option>\r\n            <ion-select-option value=\"Tratamiento\">Tratamiento</ion-select-option>\r\n          </ion-select>\r\n        </ion-item> -->\r\n        <ion-item>\r\n          <ion-label>Tipo</ion-label>\r\n          <ion-select [(ngModel)]=\"tratamiento_emergencia\" [ngModelOptions]=\"{standalone: true}\" interface=\"action-sheet\" cancelText=\"Cancelar\"\r\n            placeholder=\"Seleccionar\">\r\n            <ion-select-option *ngFor=\"let item of tratamientos\" value=\"{{item.id}}\">{{item.nombre | titlecase}}\r\n            </ion-select-option>\r\n          </ion-select>\r\n        </ion-item>\r\n        <div class=\"ion-text-center margin\">\r\n          <ion-button [disabled]=\" !form.valid \" (click)=\"createReserva()\" type=\"submit\" shape=\"round\"\r\n            id=\"principal-color-button\">\r\n            Registrar\r\n          </ion-button>\r\n        </div>\r\n      </ion-card-content>\r\n    </ion-card>\r\n  </form>\r\n</ion-content>");

/***/ }),

/***/ "iZK/":
/*!***************************************************!*\
  !*** ./src/app/create-page/create-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".margin {\n  margin-top: 25px;\n}\n\nion-input[type=date]:before {\n  content: attr(placeholder) !important;\n  color: #aaa;\n  margin-right: 0.5em;\n}\n\nion-input[type=date]:focus:before,\nion-input[type=date]:valid:before {\n  content: \"\";\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGNyZWF0ZS1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGdCQUFBO0FBQ0o7O0FBV0E7RUFDSSxxQ0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQVJKOztBQVVFOztFQUVFLFdBQUE7QUFQSiIsImZpbGUiOiJjcmVhdGUtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFyZ2luIHtcclxuICAgIG1hcmdpbi10b3A6IDI1cHg7XHJcbn1cclxuLy8gYSwgYSBkaXYsIGEgc3BhbiwgYSBpb24taWNvbiwgYSBpb24tbGFiZWwsIGJ1dHRvbiwgYnV0dG9uIGRpdiwgYnV0dG9uIHNwYW4sIGJ1dHRvbiBpb24taWNvbiwgYnV0dG9uIGlvbi1sYWJlbCwgLmlvbi10YXBwYWJsZSwgW3RhcHBhYmxlXSwgW3RhcHBhYmxlXSBkaXYsIFt0YXBwYWJsZV0gc3BhbiwgW3RhcHBhYmxlXSBpb24taWNvbiwgW3RhcHBhYmxlXSBpb24tbGFiZWwsIGlucHV0LCB0ZXh0YXJlYXtcclxuLy8gICAgIGNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIGJhY2tncm91bmQ6ICMzODgwZmY7XHJcbi8vICAgICBib3JkZXI6IG5vbmU7XHJcbi8vIH1cclxuLy8gI2lucHV0LWRhdGV7XHJcbi8vICAgICAtLWNvbG9yOiB3aGl0ZTtcclxuLy8gICAgIC0tYmFja2dyb3VuZDogIzM4ODBmZjtcclxuLy8gICAgIC8vIGJvcmRlcjogbm9uZTtcclxuLy8gfVxyXG5pb24taW5wdXRbdHlwZT1cImRhdGVcIl06YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6IGF0dHIocGxhY2Vob2xkZXIpICFpbXBvcnRhbnQ7XHJcbiAgICBjb2xvcjogI2FhYTtcclxuICAgIG1hcmdpbi1yaWdodDogMC41ZW07XHJcbiAgfVxyXG4gIGlvbi1pbnB1dFt0eXBlPVwiZGF0ZVwiXTpmb2N1czpiZWZvcmUsXHJcbiAgaW9uLWlucHV0W3R5cGU9XCJkYXRlXCJdOnZhbGlkOmJlZm9yZSB7XHJcbiAgICBjb250ZW50OiBcIlwiO1xyXG4gIH0iXX0= */");

/***/ })

}]);
//# sourceMappingURL=default~create-page-create-page-module~tab1-tab1-module~tab2-tab2-module-es2015.js.map