(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~edit-modal-edit-modal-module~tab1-tab1-module~tab2-tab2-module"],{

/***/ "4Hae":
/*!***********************************************!*\
  !*** ./src/app/edit-modal/edit-modal.page.ts ***!
  \***********************************************/
/*! exports provided: EditModalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditModalPage", function() { return EditModalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_edit_modal_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./edit-modal.page.html */ "fVjV");
/* harmony import */ var _edit_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./edit-modal.page.scss */ "wlsB");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/service.service */ "rRxC");
/* harmony import */ var _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../search-page/search-page.page */ "tw2k");
/* harmony import */ var _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../horas/horas.page */ "Y2XA");










let EditModalPage = class EditModalPage {
    constructor(modal, loading, toast, modalController, formB, alertController, service) {
        this.modal = modal;
        this.loading = loading;
        this.toast = toast;
        this.modalController = modalController;
        this.formB = formB;
        this.alertController = alertController;
        this.service = service;
        this.conditionNombreNuevo = false;
        this.conditionHora = true;
    }
    ngOnInit() {
        console.log(this.item);
        this.nombres = this.item.paciente.nombres;
        this.apellidos = this.item.paciente.apellidos;
        this.idPaciente = this.item.paciente.id;
        this.dataHour = this.item.hora;
        this.fecha = this.item.fecha;
        if (this.item) {
            this.control = true;
            this.controlHora = true;
        }
        else {
            this.control = false;
            this.controlHora = false;
        }
    }
    dismiss() {
        this.modal.dismiss();
    }
    searchCliente() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.conditionNombreNuevo = false;
            const modal = yield this.modalController.create({
                component: _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_8__["SearchPagePage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
            });
            yield modal.present();
            const { data } = yield modal.onDidDismiss();
            this.control = data.control;
            this.nombres = data.nombres;
            this.apellidos = data.apellidos;
            this.idPaciente = data.id;
        });
    }
    searchHora() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.fecha) {
                const modal = yield this.modalController.create({
                    component: _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__["HorasPage"],
                    cssClass: 'my-custom-class',
                    swipeToClose: true,
                    componentProps: {
                        'fecha': this.fecha
                    }
                });
                yield modal.present();
                const { data } = yield modal.onDidDismiss();
                if (data.dataEdit) {
                    this.controlHora = data.controlHora;
                    this.dataHour = data.data;
                }
                else {
                    this.controlHora = true;
                    this.fecha = this.item.fecha;
                }
            }
            else {
                this.presentAlertConfirm();
            }
        });
    }
    eventDate(e) {
        this.item.fecha = moment__WEBPACK_IMPORTED_MODULE_6__(e.detail.value).format('YYYY-MM-DD');
        this.conditionHora = true;
    }
    presentAlertConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Alerta!',
                message: 'Se equivoco de hora tiene que seleccionar nuevamente la fecha!!!',
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.form.reset();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    edit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            yield loading.present().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                if (this.tipo == 'reserva') {
                    this.service.editReserva(this.item.id, this.idPaciente, moment__WEBPACK_IMPORTED_MODULE_6__(this.fecha), this.dataHour).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.ok('Reserva editada');
                        loading.dismiss();
                        this.dismiss();
                    }), err => {
                        this.error('Reserva no editada');
                        loading.dismiss();
                    });
                }
                else if (this.tipo == 'emergencia') {
                    this.service.editEmergencia(this.item.id, this.idPaciente, moment__WEBPACK_IMPORTED_MODULE_6__(this.fecha), this.dataHour).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.ok('Emergencia editada');
                        this.dismiss();
                        yield loading.dismiss();
                    }), err => {
                        this.error('Emergencia no editada');
                        loading.dismiss();
                    });
                }
            }));
        });
    }
    error(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-alert'
            });
            toast.present();
        });
    }
    ok(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-success'
            });
            toast.present();
        });
    }
};
EditModalPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_7__["ServiceService"] }
];
EditModalPage.propDecorators = {
    item: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }],
    tipo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
EditModalPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-edit-modal',
        template: _raw_loader_edit_modal_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_edit_modal_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], EditModalPage);



/***/ }),

/***/ "fVjV":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/edit-modal/edit-modal.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header *ngIf=\"tipo == 'reserva'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Editar la reserva #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'reserva'\">\r\n    <!-- <form [formGroup]=\"form\"> -->\r\n\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <!-- <ion-item>\r\n        <ion-label>Hora de ingreso</ion-label>\r\n        <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"ingreso\"></ion-datetime>\r\n      </ion-item>\r\n\r\n      <ion-item>\r\n        <ion-label>Hora de llegada</ion-label>\r\n        <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"llegada\"></ion-datetime>\r\n      </ion-item> -->\r\n      <ion-item>\r\n          <ion-label>Fecha</ion-label>\r\n          <ion-input id=\"input-date\"  (ionChange)=\"eventDate($event)\" type=\"date\" [(ngModel)]=\"fecha\" min=\"{{actual\r\n          }}\" max=\"2050\" ></ion-input>\r\n        </ion-item>\r\n        <br>\r\n        <ion-item [disabled]=\"!conditionHora\" (click)=\"searchHora()\">\r\n          <ion-label >Seleccionar la hora</ion-label>\r\n        <h6 *ngIf=\"controlHora == true\">{{dataHour}}</h6>\r\n        <h6 *ngIf=\"controlHora == false\">Seleccionar la hora</h6>\r\n\r\n        </ion-item>\r\n        <br>\r\n      \r\n      <div  class=\"ion-text-center\">\r\n          <!-- <ion-item> -->\r\n            <ion-button (click)=\"searchCliente()\" expand=\"block\">\r\n              <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\r\n              <h6 *ngIf=\"control == true\">{{nombres + ' ' + apellidos}}</h6>\r\n              <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\r\n            </ion-button>\r\n            <br>\r\n        </div>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-button (click)=\"edit()\" class=\"margin\" expand=\"block\">\r\n    Editar la reserva\r\n    <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\r\n  </ion-button>\r\n<!-- </form> -->\r\n</ion-content>\r\n\r\n<ion-header *ngIf=\"tipo == 'emergencia'\" class=\"ion-no-border\">\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-title class=\"ion-text-center\">Editar la emergencia #{{item.id}}</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\" color=\"light\">\r\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content *ngIf=\"tipo == 'emergencia'\">\r\n  <ion-card>\r\n    <ion-card-content>\r\n        <ion-item>\r\n            <ion-label>Fecha</ion-label>\r\n            <ion-input id=\"input-date\"   type=\"date\" [(ngModel)]=\"fecha\" min=\"{{actual\r\n            }}\" max=\"2050\" ></ion-input>\r\n          </ion-item>\r\n      <ion-item>\r\n        <ion-label>Hora de ingreso</ion-label>\r\n        <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"dataHour\"></ion-datetime>\r\n      </ion-item>\r\n         \r\n      <div  class=\"ion-text-center\">\r\n          <!-- <ion-item> -->\r\n            <ion-button (click)=\"searchCliente()\" expand=\"block\">\r\n              <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\r\n              <h6 *ngIf=\"control == true\">{{nombres + ' ' + apellidos}}</h6>\r\n              <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\r\n            </ion-button>\r\n            <br>\r\n        </div>\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <ion-button (click)=\"edit()\" class=\"margin\" expand=\"block\">\r\n    Editar la emergencia\r\n    <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\r\n  </ion-button>\r\n\r\n</ion-content>");

/***/ }),

/***/ "wlsB":
/*!*************************************************!*\
  !*** ./src/app/edit-modal/edit-modal.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJlZGl0LW1vZGFsLnBhZ2Uuc2NzcyJ9 */");

/***/ })

}]);
//# sourceMappingURL=default~edit-modal-edit-modal-module~tab1-tab1-module~tab2-tab2-module-es2015.js.map