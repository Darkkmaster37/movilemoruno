(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["search-page-search-page-module"],{

/***/ "AVkp":
/*!***************************************************!*\
  !*** ./src/app/search-page/search-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#add {\n  color: #27ae60;\n}\n\n.buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NlYXJjaC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNlYXJjaC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhZGQge1xuICAgIGNvbG9yOiAjMjdhZTYwO1xufVxuXG4uYnV0dG9ucyB7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xufVxuIl19 */");

/***/ }),

/***/ "cZdB":
/*!**************************************************************************!*\
  !*** ./node_modules/ng2-search-filter/__ivy_ngcc__/ng2-search-filter.js ***!
  \**************************************************************************/
/*! exports provided: Ng2SearchPipeModule, Ng2SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipeModule", function() { return Ng2SearchPipeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipe", function() { return Ng2SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");



class Ng2SearchPipe {
    /**
     * @param {?} items object from array
     * @param {?} term term's search
     * @return {?}
     */
    transform(items, term) {
        if (!term || !items)
            return items;
        return Ng2SearchPipe.filter(items, term);
    }
    /**
     *
     * @param {?} items List of items to filter
     * @param {?} term  a string term to compare with every property of the list
     *
     * @return {?}
     */
    static filter(items, term) {
        const /** @type {?} */ toCompare = term.toLowerCase();
        /**
         * @param {?} item
         * @param {?} term
         * @return {?}
         */
        function checkInside(item, term) {
            for (let /** @type {?} */ property in item) {
                if (item[property] === null || item[property] == undefined) {
                    continue;
                }
                if (typeof item[property] === 'object') {
                    if (checkInside(item[property], term)) {
                        return true;
                    }
                }
                if (item[property].toString().toLowerCase().includes(toCompare)) {
                    return true;
                }
            }
            return false;
        }
        return items.filter(function (item) {
            return checkInside(item, term);
        });
    }
}
Ng2SearchPipe.ɵfac = function Ng2SearchPipe_Factory(t) { return new (t || Ng2SearchPipe)(); };
Ng2SearchPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "filter", type: Ng2SearchPipe, pure: false });
Ng2SearchPipe.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: Ng2SearchPipe, factory: Ng2SearchPipe.ɵfac });
/**
 * @nocollapse
 */
Ng2SearchPipe.ctorParameters = () => [];
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Ng2SearchPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
                name: 'filter',
                pure: false
            }]
    }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], null, null); })();

class Ng2SearchPipeModule {
}
Ng2SearchPipeModule.ɵfac = function Ng2SearchPipeModule_Factory(t) { return new (t || Ng2SearchPipeModule)(); };
Ng2SearchPipeModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: Ng2SearchPipeModule });
Ng2SearchPipeModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({});
/**
 * @nocollapse
 */
Ng2SearchPipeModule.ctorParameters = () => [];
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](Ng2SearchPipeModule, { declarations: [Ng2SearchPipe], exports: [Ng2SearchPipe] }); })();
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Ng2SearchPipeModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [Ng2SearchPipe],
                exports: [Ng2SearchPipe]
            }]
    }], null, null); })();

/**
 * Generated bundle index. Do not edit.
 */



//# sourceMappingURL=ng2-search-filter.js.map

/***/ }),

/***/ "iprd":
/*!***************************************************!*\
  !*** ./src/app/search-page/search-page.module.ts ***!
  \***************************************************/
/*! exports provided: SearchPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPagePageModule", function() { return SearchPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _search_page_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./search-page-routing.module */ "j6hZ");
/* harmony import */ var _search_page_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search-page.page */ "tw2k");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-search-filter */ "cZdB");








let SearchPagePageModule = class SearchPagePageModule {
};
SearchPagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _search_page_routing_module__WEBPACK_IMPORTED_MODULE_5__["SearchPagePageRoutingModule"],
            ng2_search_filter__WEBPACK_IMPORTED_MODULE_7__["Ng2SearchPipeModule"]
        ],
        declarations: [_search_page_page__WEBPACK_IMPORTED_MODULE_6__["SearchPagePage"]]
    })
], SearchPagePageModule);



/***/ }),

/***/ "j6hZ":
/*!***********************************************************!*\
  !*** ./src/app/search-page/search-page-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: SearchPagePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPagePageRoutingModule", function() { return SearchPagePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _search_page_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./search-page.page */ "tw2k");




const routes = [
    {
        path: '',
        component: _search_page_page__WEBPACK_IMPORTED_MODULE_3__["SearchPagePage"]
    }
];
let SearchPagePageRoutingModule = class SearchPagePageRoutingModule {
};
SearchPagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SearchPagePageRoutingModule);



/***/ }),

/***/ "kUx7":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search-page/search-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"dismiss()\" color=\"light\">\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>Buscar cliente</ion-title>\n \n  </ion-toolbar>\n</ion-header>\n<ion-searchbar placeholder=\"Buscar...\" [(ngModel)]=\"filterTerm\" animated=\"true\" [debounce]=\"2000\"\n(ionChange)=\"searchProductos()\"\n[autocomplete]=\"true\"\n\n></ion-searchbar>\n<ion-content>\n\n\n  <ion-list>\n    <ion-item *ngFor=\"let data of itemListData \" (click)=\"select(data)\">\n      <ion-label>{{(data.apellidos | titlecase) + ' ' + (data.nombres | titlecase)}}</ion-label>\n      <ion-icon id=\"add\" (click)=\"select(data)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\n    </ion-item>\n    <!-- <ion-virtual-scroll [items]=\"pacientes\" approxItemHeight=\"320px\">\n        <ion-card *virtualItem=\"let item;\">\n            <ion-item (click)=\"select(item)\">\n                <ion-label>{{(item.apellidos | titlecase) + ' ' + (item.nombres | titlecase)}}</ion-label>\n                <ion-icon id=\"add\" (click)=\"select(item)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\n              </ion-item>\n        </ion-card>\n      </ion-virtual-scroll> -->\n    \n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n      <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>");

/***/ }),

/***/ "rRxC":
/*!*********************************************!*\
  !*** ./src/app/services/service.service.ts ***!
  \*********************************************/
/*! exports provided: ServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceService", function() { return ServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let ServiceService = class ServiceService {
    constructor(http) {
        this.http = http;
        this.path = "https://c2100160.ferozo.com/api/user/";
    }
    getPacientes(page, page_size) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + 'pacientes?page=' + page + '&page_size=' + page_size + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getTratamientos() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "tratamientos", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getReservasDia() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "reservas", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    createReserva(id = "", fecha, hora, tipo, ficha, observacion, tratamiento) {
        var datoaEnviar = {
            "paciente_id": id,
            "fecha": fecha,
            "hora": hora,
            "tipo": tipo,
            // "num_ficha": ficha,
            "observacion": observacion,
            "tratamiento_id": tratamiento
        };
        return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editReserva(id, hora_ingreso, hora_llegada) {
        var datoaEnviar = {
            "reserva_id": id,
            "hora_ingreso": hora_ingreso,
            "hora_llegada": hora_llegada
        };
        return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    deleteReserva(id) {
        var datoaEnviar = {
            "reserva_id": id,
        };
        return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getEmergenciasDia() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "emergencias", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    createEmergencia(id = '', fecha, hora, tipo, observacion, tratamiento) {
        var datoaEnviar = {
            "paciente_id": id,
            "fecha": fecha,
            "hora": hora,
            "tipo": tipo,
            "observacion": observacion,
            "tratamiento_id": tratamiento
        };
        return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editEmergencia(id, hora_ingreso) {
        var datoaEnviar = {
            "emergencia_id": id,
            "hora_ingreso": hora_ingreso
        };
        return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    deleteEmergencia(id) {
        var datoaEnviar = {
            "emergencia_id": id,
        };
        return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchProductos(nombre) {
        var datoaEnviar = {
            "nombre": nombre,
        };
        return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchReserva(fecha) {
        var datoaEnviar = {
            "fecha": fecha,
        };
        return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchReservaTratamiento(fecha) {
        var datoaEnviar = {
            "fecha": fecha,
        };
        return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ServiceService);



/***/ }),

/***/ "tw2k":
/*!*************************************************!*\
  !*** ./src/app/search-page/search-page.page.ts ***!
  \*************************************************/
/*! exports provided: SearchPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPagePage", function() { return SearchPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./search-page.page.html */ "kUx7");
/* harmony import */ var _search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-page.page.scss */ "AVkp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");






let SearchPagePage = class SearchPagePage {
    constructor(service, loading, modal) {
        this.service = service;
        this.loading = loading;
        this.modal = modal;
        this.itemListData = [];
        this.page_number = 1;
        this.page_limit = 100;
    }
    ngOnInit() {
        this.getPacientes(false, "");
    }
    getPacientes(isFirstLoad, event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // const loading = await this.loading.create({
            //   spinner: 'bubbles',
            //   translucent: true,
            // });
            // await loading.present().then(async () => {
            this.service.getPacientes(this.page_number, this.page_limit).subscribe((data) => {
                data = data.data;
                for (let i = 0; i < data.length; i++) {
                    this.itemListData.push(data[i]);
                }
                if (isFirstLoad)
                    event.target.complete();
                this.page_number++;
                // loading.present();
            }, error => {
                //  loading.present();
            });
            // })
        });
    }
    doInfinite(event) {
        this.getPacientes(true, event);
    }
    select(data) {
        this.modal.dismiss({
            id: data.id,
            nombres: data.nombres,
            apellidos: data.apellidos,
            control: true
        });
    }
    dismiss() {
        this.modal.dismiss({
            id: '',
            nombres: '',
            apellidos: '',
            control: false
        });
    }
    searchProductos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.filterTerm === '') {
                this.page_number = 1;
                this.getPacientes(false, "");
            }
            else {
                this.itemListData = [];
                const loading = yield this.loading.create({
                    spinner: 'bubbles',
                    translucent: true,
                });
                yield loading.present().then(() => {
                    this.service.searchProductos(this.filterTerm).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.itemListData = data.data;
                        yield loading.dismiss();
                    }), err => {
                        loading.dismiss();
                    });
                });
            }
        });
    }
};
SearchPagePage.ctorParameters = () => [
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
SearchPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-search-page',
        template: _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SearchPagePage);



/***/ })

}]);
//# sourceMappingURL=search-page-search-page-module-es2015.js.map