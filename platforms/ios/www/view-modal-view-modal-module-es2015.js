(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["view-modal-view-modal-module"],{

/***/ "7uT/":
/*!*********************************************************!*\
  !*** ./src/app/view-modal/view-modal-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ViewModalPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewModalPageRoutingModule", function() { return ViewModalPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _view_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./view-modal.page */ "9Tgf");




const routes = [
    {
        path: '',
        component: _view_modal_page__WEBPACK_IMPORTED_MODULE_3__["ViewModalPage"]
    }
];
let ViewModalPageRoutingModule = class ViewModalPageRoutingModule {
};
ViewModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ViewModalPageRoutingModule);



/***/ }),

/***/ "hOwK":
/*!*************************************************!*\
  !*** ./src/app/view-modal/view-modal.module.ts ***!
  \*************************************************/
/*! exports provided: ViewModalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewModalPageModule", function() { return ViewModalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _view_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./view-modal-routing.module */ "7uT/");
/* harmony import */ var _view_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./view-modal.page */ "9Tgf");







let ViewModalPageModule = class ViewModalPageModule {
};
ViewModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _view_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViewModalPageRoutingModule"]
        ],
        declarations: [_view_modal_page__WEBPACK_IMPORTED_MODULE_6__["ViewModalPage"]]
    })
], ViewModalPageModule);



/***/ }),

/***/ "rRxC":
/*!*********************************************!*\
  !*** ./src/app/services/service.service.ts ***!
  \*********************************************/
/*! exports provided: ServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceService", function() { return ServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "kU1M");





let ServiceService = class ServiceService {
    constructor(http) {
        this.http = http;
        this.path = "https://c2100160.ferozo.com/api/user/";
    }
    getPacientes(page, page_size) {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + 'pacientes?page=' + page + '&page_size=' + page_size + '', this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getTratamientos() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "tratamientos", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getReservasDia() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "reservas", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    createReserva(id = "", fecha, hora, tipo, ficha, observacion, tratamiento) {
        var datoaEnviar = {
            "paciente_id": id,
            "fecha": fecha,
            "hora": hora,
            "tipo": tipo,
            // "num_ficha": ficha,
            "observacion": observacion,
            "tratamiento_id": tratamiento
        };
        return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editReserva(id, hora_ingreso, hora_llegada) {
        var datoaEnviar = {
            "reserva_id": id,
            "hora_ingreso": hora_ingreso,
            "hora_llegada": hora_llegada
        };
        return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    deleteReserva(id) {
        var datoaEnviar = {
            "reserva_id": id,
        };
        return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    getEmergenciasDia() {
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
            })
        };
        return this.http.get(this.path + "emergencias", this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    createEmergencia(id = '', fecha, hora, tipo, observacion, tratamiento) {
        var datoaEnviar = {
            "paciente_id": id,
            "fecha": fecha,
            "hora": hora,
            "tipo": tipo,
            "observacion": observacion,
            "tratamiento_id": tratamiento
        };
        return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    editEmergencia(id, hora_ingreso) {
        var datoaEnviar = {
            "emergencia_id": id,
            "hora_ingreso": hora_ingreso
        };
        return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    deleteEmergencia(id) {
        var datoaEnviar = {
            "emergencia_id": id,
        };
        return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchProductos(nombre) {
        var datoaEnviar = {
            "nombre": nombre,
        };
        return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchReserva(fecha) {
        var datoaEnviar = {
            "fecha": fecha,
        };
        return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
    searchReservaTratamiento(fecha) {
        var datoaEnviar = {
            "fecha": fecha,
        };
        return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])((data) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])((err) => {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
        }));
    }
};
ServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ServiceService);



/***/ })

}]);
//# sourceMappingURL=view-modal-view-modal-module-es2015.js.map