(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["tab1-tab1-module"], {
    /***/
    "8MT7":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/tab1/tab1.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function MT7(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header class=\"ion-no-border\">\n  <ion-toolbar color=\"primary\">\n    <!-- <br> -->\n    <ion-title class=\"ion-text-center\">Reservas del día</ion-title>\n    <!-- <ion-button (click)=\"createReserva()\" class=\"margin\" expand=\"block\" color=\"success\">\n        Crear Reserva\n        <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>\n      </ion-button> -->\n      <ion-buttons slot=\"end\">\n          <ion-button (click)=\"createReserva()\" color=\"light\">\n            <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"add\"></ion-icon>\n          </ion-button>\n        </ion-buttons>\n        \n  </ion-toolbar>\n  <ion-grid fixed>\n   \n  <ion-row>\n      <ion-col  class=\"ion-text-center\"  size=\"6\">\n        <br>\n        <!-- <ion-item>\n          <ion-label position=\"floating\">Fecha de Reservas</ion-label>\n        <ion-input id=\"input-date\" type=\"date\" [(ngModel)]=\"fecha\" ></ion-input>\n      </ion-item> -->\n        \n        <ion-label class=\"label-date\" >Fecha de Reservas</ion-label>\n        <!-- <ion-input id=\"input-date\" class=\"custom-class\" type=\"date\" [(ngModel)]=\"fecha\" placeholder=\"DD/MM/YYYY\"></ion-input> -->\n        <input min=\"{{actual}}\" type=\"date\"  [(ngModel)]=\"fecha\" />\n        <!-- <input id=\"input-date\" [(ngModel)]=\"fecha\" type=\"date\" placeholder=\"DD/MM/YYYY\"></input> -->\n          <!-- <ion-datetime [(ngModel)]=\"fecha\" display-format=\"DD-MM-YYYY\" placeholder=\"DD/MM/YYYY\"></ion-datetime> -->\n      </ion-col>\n      <ion-col size=\"6\">\n          <ion-button (click)=\"searchFilterReserva()\" expand=\"block\"  shape=\"round\">\n              Buscar\n            </ion-button>\n            <p *ngIf=\"buttonOn\" >\n            <ion-button (click)=\"reset()\" expand=\"block\" shape=\"round\">\n                Limpiar\n              </ion-button>\n            </p>\n      </ion-col>\n    </ion-row></ion-grid>\n</ion-header>\n\n<ion-content >\n\n<!-- <div>\n<br> -->\n  <!-- <ion-grid fixed>\n    <ion-row>\n      <ion-col  class=\"ion-text-center\"  size=\"6\">\n        <ion-label position=\"inline\">Fecha de Reservas</ion-label>\n          <ion-datetime [(ngModel)]=\"fecha\" display-format=\"DD-MM-YYYY\" placeholder=\"DD/MM/YYYY\"></ion-datetime>\n      </ion-col>\n      <ion-col size=\"6\">\n          <ion-button (click)=\"searchFilterReserva()\" expand=\"block\"  shape=\"round\">\n              Buscar\n            </ion-button>\n            <p *ngIf=\"buttonOn\" >\n            <ion-button (click)=\"reset()\" expand=\"block\" shape=\"round\">\n                Limpiar\n              </ion-button>\n            </p>\n      </ion-col>\n    </ion-row>\n  </ion-grid> -->\n\n  \n\n  <ion-list *ngIf=\"reservas.length == 0 && !det\">\n    <ion-button expand=\"block\">\n      Cargando\n      <ion-icon name=\"reload\" style=\"font-size: 25px;\" slot=\"end\" #loadingIcon></ion-icon>\n    </ion-button>\n  </ion-list>\n\n  <ion-list *ngIf=\"reservas.length == 0 && det\">\n    <ion-item>\n      <span class=\"text danger\">No hay reservas del día</span>\n    </ion-item>\n  </ion-list>\n\n\n  <ion-list *ngIf=\"reservas.length >= 1 && det\">\n    <ion-card *ngFor=\"let item of reservas\">\n      <div class=\"ion-text-center\">\n        <!-- <h3><b>  Num. Ficha:  {{item.num_ficha}}</b></h3>  -->\n      </div>\n      <ion-card-header>\n        <!-- <ion-card-title>Awesome Title</ion-card-title> -->\n      </ion-card-header>\n      <ion-card-content>\n          <div>\n              <ion-item>\n                  <ion-avatar slot=\"start\">\n                      <img src=\"assets/user.png\" />\n                    </ion-avatar>\n                    <ion-label>\n                        <p><b>{{item.paciente.nombre}}</b> </p>\n                        <p *ngIf=\"item.observacion\" ><b>{{item.observacion}}</b> </p>\n\n                        Tipo:  {{item.tipo}}\n\n                        </ion-label>\n              </ion-item>\n              \n            </div>\n      <div>\n        <br>\n        <ion-grid fixed>\n          <ion-row>\n            <ion-col size=\"6\">\n             <b>Fecha de Reserva</b> \n            <p>{{item.fecha}}</p> \n            <br>\n            <b>Hora de cita</b> \n            <p>{{item.hora}}</p>\n            </ion-col>\n            <ion-col *ngIf=\"item.tratamiento\"  size=\"6\">\n                <b>Tipo de tratamiento</b> \n\n            <p>  {{item.tratamiento}}</p>\n\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n      <br>\n     \n      <ion-grid  fixed>\n        <ion-row class=\"ion-text-center\">\n          <ion-col size=\"4\">\n              <ion-icon id=\"add\" (click)=\"viewReserva(item.id,item)\" name=\"eye\" class=\"buttons\"></ion-icon>\n\n          </ion-col>\n          <ion-col size=\"4\">\n              <ion-icon id=\"edit-menu\" (click)=\"editReserva(item.id)\" name=\"create\" class=\"buttons\"></ion-icon>\n\n          </ion-col>\n          <ion-col size=\"4\">\n              <ion-icon id=\"eliminar\" (click)=\"deleteReserva(item.id)\" name=\"trash\" class=\"buttons\"></ion-icon>\n\n          </ion-col>\n\n        </ion-row>\n      </ion-grid>\n      </ion-card-content>\n        <!-- <ion-item >\n\n            <div slot=\"end\">\n              <ion-icon id=\"add\" (click)=\"viewReserva(item.id)\" name=\"eye\" class=\"buttons\"></ion-icon>\n              <ion-icon id=\"edit-menu\" (click)=\"editReserva(item.id)\" name=\"create\" class=\"buttons\"></ion-icon>\n              <ion-icon id=\"eliminar\" (click)=\"deleteReserva(item.id)\" name=\"trash\" class=\"buttons\"></ion-icon>\n            </div>\n          </ion-item> -->\n    </ion-card>\n   \n  </ion-list>\n\n</ion-content>";
      /***/
    },

    /***/
    "Mzl2":
    /*!***********************************!*\
      !*** ./src/app/tab1/tab1.page.ts ***!
      \***********************************/

    /*! exports provided: Tab1Page */

    /***/
    function Mzl2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab1Page", function () {
        return Tab1Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./tab1.page.html */
      "8MT7");
      /* harmony import */


      var _tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./tab1.page.scss */
      "rWyk");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../services/service.service */
      "rRxC");
      /* harmony import */


      var _view_modal_view_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../view-modal/view-modal.page */
      "9Tgf");
      /* harmony import */


      var _edit_modal_edit_modal_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../edit-modal/edit-modal.page */
      "4Hae");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _create_page_create_page_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! ../create-page/create-page.page */
      "LXxN");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! moment */
      "wd/R");
      /* harmony import */


      var moment__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_10__);

      var Tab1Page = /*#__PURE__*/function () {
        // type:any;
        function Tab1Page(service, animationCtrl, loading, modalController, alert, toast, router) {
          _classCallCheck(this, Tab1Page);

          this.service = service;
          this.animationCtrl = animationCtrl;
          this.loading = loading;
          this.modalController = modalController;
          this.alert = alert;
          this.toast = toast;
          this.router = router;
          this.actual = moment__WEBPACK_IMPORTED_MODULE_10__().format('YYYY-MM-DD');
          this.getReservas();
        }

        _createClass(Tab1Page, [{
          key: "ionViewDidEnter",
          value: function ionViewDidEnter() {
            this.getReservas();
            this.fecha = '';
          }
        }, {
          key: "getReservas",
          value: function getReservas() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this = this;

              var loading;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.fecha = '';
                      this.buttonOn = true;
                      this.det = '';
                      this.reservas = [];
                      _context3.next = 6;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 6:
                      loading = _context3.sent;
                      this.animate();
                      _context3.next = 10;
                      return loading.present().then(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                          var _this2 = this;

                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                            while (1) {
                              switch (_context2.prev = _context2.next) {
                                case 0:
                                  this.service.getReservasDia().subscribe(function (data) {
                                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                      return regeneratorRuntime.wrap(function _callee$(_context) {
                                        while (1) {
                                          switch (_context.prev = _context.next) {
                                            case 0:
                                              this.reservas = data.data;
                                              this.det = 'lleno';
                                              loading.dismiss();

                                            case 3:
                                            case "end":
                                              return _context.stop();
                                          }
                                        }
                                      }, _callee, this);
                                    }));
                                  }, function (err) {
                                    loading.dismiss();
                                  });

                                case 1:
                                case "end":
                                  return _context2.stop();
                              }
                            }
                          }, _callee2, this);
                        }));
                      });

                    case 10:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "searchFilterReserva",
          value: function searchFilterReserva() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var _this3 = this;

              var fechaFilter, loading;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      fechaFilter = moment__WEBPACK_IMPORTED_MODULE_10__(this.fecha).format('YYYY-MM-DD');
                      this.det = '';
                      this.reservas = [];
                      _context6.next = 5;
                      return this.loading.create({
                        spinner: 'bubbles',
                        translucent: true
                      });

                    case 5:
                      loading = _context6.sent;
                      this.animate();
                      _context6.next = 9;
                      return loading.present().then(function () {
                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                          var _this4 = this;

                          return regeneratorRuntime.wrap(function _callee5$(_context5) {
                            while (1) {
                              switch (_context5.prev = _context5.next) {
                                case 0:
                                  this.service.searchReserva(fechaFilter).subscribe(function (data) {
                                    return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                                      return regeneratorRuntime.wrap(function _callee4$(_context4) {
                                        while (1) {
                                          switch (_context4.prev = _context4.next) {
                                            case 0:
                                              this.reservas = data.data;
                                              this.det = 'lleno';
                                              loading.dismiss();

                                            case 3:
                                            case "end":
                                              return _context4.stop();
                                          }
                                        }
                                      }, _callee4, this);
                                    }));
                                  }, function (err) {
                                    loading.dismiss();
                                  });

                                case 1:
                                case "end":
                                  return _context5.stop();
                              }
                            }
                          }, _callee5, this);
                        }));
                      });

                    case 9:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "reset",
          value: function reset() {
            this.buttonOn = false;
            this.fecha = '';
            this.getReservas();
          }
        }, {
          key: "animate",
          value: function animate() {
            var loadingAnimation = this.animationCtrl.create('loading-animation').addElement(this.loadingIcon.nativeElement).duration(1500).iterations(3).fromTo('transform', 'rotate(0deg)', 'rotate(360deg)');
            loadingAnimation.play();
          }
        }, {
          key: "viewReserva",
          value: function viewReserva(id, item) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              var modal;
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.modalController.create({
                        component: _view_modal_view_modal_page__WEBPACK_IMPORTED_MODULE_6__["ViewModalPage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'id': id,
                          'tipo': 'reserva',
                          'item': item
                        }
                      });

                    case 2:
                      modal = _context7.sent;
                      _context7.next = 5;
                      return modal.present();

                    case 5:
                      return _context7.abrupt("return", _context7.sent);

                    case 6:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "editReserva",
          value: function editReserva(id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var modal;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      _context8.next = 2;
                      return this.modalController.create({
                        component: _edit_modal_edit_modal_page__WEBPACK_IMPORTED_MODULE_7__["EditModalPage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'id': id,
                          'tipo': 'reserva'
                        }
                      });

                    case 2:
                      modal = _context8.sent;
                      _context8.next = 5;
                      return modal.present();

                    case 5:
                      return _context8.abrupt("return", _context8.sent);

                    case 6:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }, {
          key: "deleteReserva",
          value: function deleteReserva(id) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var _this5 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      _context9.next = 2;
                      return this.alert.create({
                        cssClass: 'my-custom-class',
                        header: 'Confirm!',
                        message: '¿Eliminar Reserva?',
                        buttons: [{
                          text: 'Cancel',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler(blah) {}
                        }, {
                          text: 'Okay',
                          handler: function handler() {
                            _this5.service.deleteReserva(id).subscribe(function (data) {
                              _this5.ok('Reserva eliminada');

                              _this5.getReservas();
                            }, function (err) {
                              _this5.error(err);
                            });
                          }
                        }]
                      });

                    case 2:
                      alert = _context9.sent;
                      _context9.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "createReserva",
          value: function createReserva() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              var _this6 = this;

              var modal;
              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      _context10.next = 2;
                      return this.modalController.create({
                        component: _create_page_create_page_page__WEBPACK_IMPORTED_MODULE_9__["CreatePagePage"],
                        cssClass: 'my-custom-class',
                        swipeToClose: true,
                        componentProps: {
                          'tipo': 'reserva'
                        }
                      });

                    case 2:
                      modal = _context10.sent;
                      modal.onDidDismiss().then(function () {
                        _this6.getReservas();
                      });
                      _context10.next = 6;
                      return modal.present();

                    case 6:
                      return _context10.abrupt("return", _context10.sent);

                    case 7:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10, this);
            }));
          }
        }, {
          key: "ok",
          value: function ok(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
              var toast;
              return regeneratorRuntime.wrap(function _callee11$(_context11) {
                while (1) {
                  switch (_context11.prev = _context11.next) {
                    case 0:
                      _context11.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 3000,
                        cssClass: 'my-custom-class-success'
                      });

                    case 2:
                      toast = _context11.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context11.stop();
                  }
                }
              }, _callee11, this);
            }));
          }
        }, {
          key: "error",
          value: function error(m) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
              var toast;
              return regeneratorRuntime.wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      _context12.next = 2;
                      return this.toast.create({
                        message: m,
                        duration: 3000,
                        cssClass: 'my-custom-class-alert'
                      });

                    case 2:
                      toast = _context12.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12, this);
            }));
          }
        }]);

        return Tab1Page;
      }();

      Tab1Page.ctorParameters = function () {
        return [{
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AnimationController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"]
        }];
      };

      Tab1Page.propDecorators = {
        loadingIcon: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['loadingIcon', {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ElementRef"]
          }]
        }]
      };
      Tab1Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-tab1',
        template: _raw_loader_tab1_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_tab1_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], Tab1Page);
      /***/
    },

    /***/
    "XOzS":
    /*!*********************************************!*\
      !*** ./src/app/tab1/tab1-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: Tab1PageRoutingModule */

    /***/
    function XOzS(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab1PageRoutingModule", function () {
        return Tab1PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _tab1_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./tab1.page */
      "Mzl2");

      var routes = [{
        path: '',
        component: _tab1_page__WEBPACK_IMPORTED_MODULE_3__["Tab1Page"]
      }];

      var Tab1PageRoutingModule = function Tab1PageRoutingModule() {
        _classCallCheck(this, Tab1PageRoutingModule);
      };

      Tab1PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Tab1PageRoutingModule);
      /***/
    },

    /***/
    "rWyk":
    /*!*************************************!*\
      !*** ./src/app/tab1/tab1.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function rWyk(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n\n.label-date {\n  color: black;\n}\n\ninput[type=date]:before {\n  content: attr(placeholder) !important;\n  color: #aaa;\n  margin-right: 0.5em;\n}\n\ninput[type=date]:focus:before,\ninput[type=date]:valid:before {\n  content: \"\";\n}\n\nion-datetime {\n  color: black;\n}\n\n.date-placeholder {\n  display: inline-block;\n  position: absolute;\n  text-align: left;\n  color: #aaa;\n  background-color: white;\n  cursor: text;\n  /* Customize this stuff based on your styles */\n  top: 4px;\n  left: 4px;\n  right: 4px;\n  bottom: 4px;\n  line-height: 32px;\n  padding-left: 12px;\n}\n\nion-grid {\n  background: white;\n}\n\n#add {\n  color: #27ae60;\n}\n\n#edit-menu {\n  color: #2980b9;\n}\n\n#eliminar {\n  color: #c0392b;\n}\n\nion-list {\n  padding-top: 30px;\n}\n\n.text {\n  display: inline-block;\n  vertical-align: middle;\n}\n\n.danger {\n  color: red;\n  font-weight: bolder;\n}\n\n.margin {\n  margin-top: 25px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3RhYjEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBQ0E7RUFDSSxZQUFBO0FBRUo7O0FBS0E7RUFDSSxxQ0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQUZKOztBQUlFOztFQUVFLFdBQUE7QUFESjs7QUFLQTtFQUNJLFlBQUE7QUFGSjs7QUFJQTtFQUNJLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSw4Q0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtBQURKOztBQUdBO0VBQ0ksY0FBQTtBQUFKOztBQUVBO0VBQ0ksY0FBQTtBQUNKOztBQUNBO0VBQ0ksY0FBQTtBQUVKOztBQUFBO0VBQ0ksaUJBQUE7QUFHSjs7QUFEQTtFQUNJLHFCQUFBO0VBQ0Esc0JBQUE7QUFJSjs7QUFGQTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtBQUtKOztBQUZBO0VBQ0ksZ0JBQUE7QUFLSiIsImZpbGUiOiJ0YWIxLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b25zIHtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG59XG4ubGFiZWwtZGF0ZXtcbiAgICBjb2xvcjogYmxhY2s7XG59XG4vLyAjaW5wdXQtZGF0ZXtcbi8vICAgICAtLWNvbG9yOiB3aGl0ZTtcbi8vICAgICAtLWJhY2tncm91bmQ6ICMzODgwZmY7XG4vLyAgICAgLy8gYm9yZGVyOiBub25lO1xuLy8gfVxuaW5wdXRbdHlwZT1cImRhdGVcIl06YmVmb3JlIHtcbiAgICBjb250ZW50OiBhdHRyKHBsYWNlaG9sZGVyKSAhaW1wb3J0YW50O1xuICAgIGNvbG9yOiAjYWFhO1xuICAgIG1hcmdpbi1yaWdodDogMC41ZW07XG4gIH1cbiAgaW5wdXRbdHlwZT1cImRhdGVcIl06Zm9jdXM6YmVmb3JlLFxuICBpbnB1dFt0eXBlPVwiZGF0ZVwiXTp2YWxpZDpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXCI7XG4gIH1cblxuICBcbmlvbi1kYXRldGltZXtcbiAgICBjb2xvcjogYmxhY2s7IFxufVxuLmRhdGUtcGxhY2Vob2xkZXIge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBjb2xvcjogI2FhYTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBjdXJzb3I6IHRleHQ7XG4gICAgLyogQ3VzdG9taXplIHRoaXMgc3R1ZmYgYmFzZWQgb24geW91ciBzdHlsZXMgKi9cbiAgICB0b3A6IDRweDtcbiAgICBsZWZ0OiA0cHg7XG4gICAgcmlnaHQ6IDRweDtcbiAgICBib3R0b206IDRweDtcbiAgICBsaW5lLWhlaWdodDogMzJweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDEycHg7XG59XG5cbmlvbi1ncmlke1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuI2FkZCB7XG4gICAgY29sb3I6ICMyN2FlNjA7XG59XG4jZWRpdC1tZW51IHtcbiAgICBjb2xvcjogIzI5ODBiOTtcbn1cbiNlbGltaW5hciB7XG4gICAgY29sb3I6ICNjMDM5MmI7XG59XG5pb24tbGlzdCB7XG4gICAgcGFkZGluZy10b3A6IDMwcHg7XG59XG4udGV4dCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG59XG4uZGFuZ2VyIHtcbiAgICBjb2xvcjogcmVkO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbi5tYXJnaW4ge1xuICAgIG1hcmdpbi10b3A6IDI1cHg7XG59XG4iXX0= */";
      /***/
    },

    /***/
    "tmrb":
    /*!*************************************!*\
      !*** ./src/app/tab1/tab1.module.ts ***!
      \*************************************/

    /*! exports provided: Tab1PageModule */

    /***/
    function tmrb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Tab1PageModule", function () {
        return Tab1PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _tab1_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./tab1.page */
      "Mzl2");
      /* harmony import */


      var _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../explore-container/explore-container.module */
      "qtYk");
      /* harmony import */


      var _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ./tab1-routing.module */
      "XOzS");

      var Tab1PageModule = function Tab1PageModule() {
        _classCallCheck(this, Tab1PageModule);
      };

      Tab1PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        imports: [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["IonicModule"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _explore_container_explore_container_module__WEBPACK_IMPORTED_MODULE_6__["ExploreContainerComponentModule"], _tab1_routing_module__WEBPACK_IMPORTED_MODULE_7__["Tab1PageRoutingModule"]],
        declarations: [_tab1_page__WEBPACK_IMPORTED_MODULE_5__["Tab1Page"]]
      })], Tab1PageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=tab1-tab1-module-es5.js.map