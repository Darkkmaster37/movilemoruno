(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["view-modal-view-modal-module"], {
    /***/
    "7uT/":
    /*!*********************************************************!*\
      !*** ./src/app/view-modal/view-modal-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: ViewModalPageRoutingModule */

    /***/
    function uT(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewModalPageRoutingModule", function () {
        return ViewModalPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _view_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./view-modal.page */
      "9Tgf");

      var routes = [{
        path: '',
        component: _view_modal_page__WEBPACK_IMPORTED_MODULE_3__["ViewModalPage"]
      }];

      var ViewModalPageRoutingModule = function ViewModalPageRoutingModule() {
        _classCallCheck(this, ViewModalPageRoutingModule);
      };

      ViewModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ViewModalPageRoutingModule);
      /***/
    },

    /***/
    "hOwK":
    /*!*************************************************!*\
      !*** ./src/app/view-modal/view-modal.module.ts ***!
      \*************************************************/

    /*! exports provided: ViewModalPageModule */

    /***/
    function hOwK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViewModalPageModule", function () {
        return ViewModalPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _view_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./view-modal-routing.module */
      "7uT/");
      /* harmony import */


      var _view_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./view-modal.page */
      "9Tgf");

      var ViewModalPageModule = function ViewModalPageModule() {
        _classCallCheck(this, ViewModalPageModule);
      };

      ViewModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _view_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViewModalPageRoutingModule"]],
        declarations: [_view_modal_page__WEBPACK_IMPORTED_MODULE_6__["ViewModalPage"]]
      })], ViewModalPageModule);
      /***/
    },

    /***/
    "rRxC":
    /*!*********************************************!*\
      !*** ./src/app/services/service.service.ts ***!
      \*********************************************/

    /*! exports provided: ServiceService */

    /***/
    function rRxC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ServiceService", function () {
        return ServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var ServiceService = /*#__PURE__*/function () {
        function ServiceService(http) {
          _classCallCheck(this, ServiceService);

          this.http = http;
          this.path = "https://c2100160.ferozo.com/api/user/";
        }

        _createClass(ServiceService, [{
          key: "getPacientes",
          value: function getPacientes(page, page_size) {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + 'pacientes?page=' + page + '&page_size=' + page_size + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getTratamientos",
          value: function getTratamientos() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "tratamientos", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getReservasDia",
          value: function getReservasDia() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "reservas", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "createReserva",
          value: function createReserva() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
            var fecha = arguments.length > 1 ? arguments[1] : undefined;
            var hora = arguments.length > 2 ? arguments[2] : undefined;
            var tipo = arguments.length > 3 ? arguments[3] : undefined;
            var ficha = arguments.length > 4 ? arguments[4] : undefined;
            var observacion = arguments.length > 5 ? arguments[5] : undefined;
            var tratamiento = arguments.length > 6 ? arguments[6] : undefined;
            var datoaEnviar = {
              "paciente_id": id,
              "fecha": fecha,
              "hora": hora,
              "tipo": tipo,
              // "num_ficha": ficha,
              "observacion": observacion,
              "tratamiento_id": tratamiento
            };
            return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "editReserva",
          value: function editReserva(id, hora_ingreso, hora_llegada) {
            var datoaEnviar = {
              "reserva_id": id,
              "hora_ingreso": hora_ingreso,
              "hora_llegada": hora_llegada
            };
            return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "deleteReserva",
          value: function deleteReserva(id) {
            var datoaEnviar = {
              "reserva_id": id
            };
            return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getEmergenciasDia",
          value: function getEmergenciasDia() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "emergencias", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "createEmergencia",
          value: function createEmergencia() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            var fecha = arguments.length > 1 ? arguments[1] : undefined;
            var hora = arguments.length > 2 ? arguments[2] : undefined;
            var tipo = arguments.length > 3 ? arguments[3] : undefined;
            var observacion = arguments.length > 4 ? arguments[4] : undefined;
            var tratamiento = arguments.length > 5 ? arguments[5] : undefined;
            var datoaEnviar = {
              "paciente_id": id,
              "fecha": fecha,
              "hora": hora,
              "tipo": tipo,
              "observacion": observacion,
              "tratamiento_id": tratamiento
            };
            return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "editEmergencia",
          value: function editEmergencia(id, hora_ingreso) {
            var datoaEnviar = {
              "emergencia_id": id,
              "hora_ingreso": hora_ingreso
            };
            return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "deleteEmergencia",
          value: function deleteEmergencia(id) {
            var datoaEnviar = {
              "emergencia_id": id
            };
            return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchProductos",
          value: function searchProductos(nombre) {
            var datoaEnviar = {
              "nombre": nombre
            };
            return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchReserva",
          value: function searchReserva(fecha) {
            var datoaEnviar = {
              "fecha": fecha
            };
            return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchReservaTratamiento",
          value: function searchReservaTratamiento(fecha) {
            var datoaEnviar = {
              "fecha": fecha
            };
            return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }]);

        return ServiceService;
      }();

      ServiceService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      ServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ServiceService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=view-modal-view-modal-module-es5.js.map