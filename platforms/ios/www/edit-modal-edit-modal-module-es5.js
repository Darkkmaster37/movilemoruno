(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["edit-modal-edit-modal-module"], {
    /***/
    "MEue":
    /*!*********************************************************!*\
      !*** ./src/app/edit-modal/edit-modal-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: EditModalPageRoutingModule */

    /***/
    function MEue(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditModalPageRoutingModule", function () {
        return EditModalPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _edit_modal_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./edit-modal.page */
      "4Hae");

      var routes = [{
        path: '',
        component: _edit_modal_page__WEBPACK_IMPORTED_MODULE_3__["EditModalPage"]
      }];

      var EditModalPageRoutingModule = function EditModalPageRoutingModule() {
        _classCallCheck(this, EditModalPageRoutingModule);
      };

      EditModalPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditModalPageRoutingModule);
      /***/
    },

    /***/
    "tNOb":
    /*!*************************************************!*\
      !*** ./src/app/edit-modal/edit-modal.module.ts ***!
      \*************************************************/

    /*! exports provided: EditModalPageModule */

    /***/
    function tNOb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditModalPageModule", function () {
        return EditModalPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _edit_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./edit-modal-routing.module */
      "MEue");
      /* harmony import */


      var _edit_modal_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./edit-modal.page */
      "4Hae");

      var EditModalPageModule = function EditModalPageModule() {
        _classCallCheck(this, EditModalPageModule);
      };

      EditModalPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _edit_modal_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditModalPageRoutingModule"]],
        declarations: [_edit_modal_page__WEBPACK_IMPORTED_MODULE_6__["EditModalPage"]]
      })], EditModalPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=edit-modal-edit-modal-module-es5.js.map