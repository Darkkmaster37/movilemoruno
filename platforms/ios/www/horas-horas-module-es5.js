(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["horas-horas-module"], {
    /***/
    "8MI0":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/horas/horas.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function MI0(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"end\">\n          <ion-button (click)=\"dismiss()\" color=\"light\">\n            <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\n          </ion-button>\n        </ion-buttons>\n    <ion-title class=\"ion-text-center\">Seleccionar hora</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"ion-text-center\">\n    <h3>Horas Disponibles</h3>\n    <br>\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"4\"  *ngFor=\"let item of horasTime\" >\n            <ion-chip  (click)=\"select(item)\" color=\"primary\" mode=\"ios\" outline=\"true\">\n                <ion-icon name=\"time\"></ion-icon>\n                <ion-label>{{item}}</ion-label>\n              </ion-chip>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-card>\n        \n    </ion-card>\n    <br>\n  </div>\n \n\n</ion-content>\n";
      /***/
    },

    /***/
    "Dj7C":
    /*!***********************************************!*\
      !*** ./src/app/horas/horas-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: HorasPageRoutingModule */

    /***/
    function Dj7C(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HorasPageRoutingModule", function () {
        return HorasPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _horas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./horas.page */
      "Y2XA");

      var routes = [{
        path: '',
        component: _horas_page__WEBPACK_IMPORTED_MODULE_3__["HorasPage"]
      }];

      var HorasPageRoutingModule = function HorasPageRoutingModule() {
        _classCallCheck(this, HorasPageRoutingModule);
      };

      HorasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HorasPageRoutingModule);
      /***/
    },

    /***/
    "R2Z3":
    /*!***************************************!*\
      !*** ./src/app/horas/horas.module.ts ***!
      \***************************************/

    /*! exports provided: HorasPageModule */

    /***/
    function R2Z3(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HorasPageModule", function () {
        return HorasPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _horas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./horas-routing.module */
      "Dj7C");
      /* harmony import */


      var _horas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./horas.page */
      "Y2XA");

      var HorasPageModule = function HorasPageModule() {
        _classCallCheck(this, HorasPageModule);
      };

      HorasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _horas_routing_module__WEBPACK_IMPORTED_MODULE_5__["HorasPageRoutingModule"]],
        declarations: [_horas_page__WEBPACK_IMPORTED_MODULE_6__["HorasPage"]]
      })], HorasPageModule);
      /***/
    },

    /***/
    "Y2XA":
    /*!*************************************!*\
      !*** ./src/app/horas/horas.page.ts ***!
      \*************************************/

    /*! exports provided: HorasPage */

    /***/
    function Y2XA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HorasPage", function () {
        return HorasPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _raw_loader_horas_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! raw-loader!./horas.page.html */
      "8MI0");
      /* harmony import */


      var _horas_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./horas.page.scss */
      "bECs");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "TEn/");
      /* harmony import */


      var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../services/service.service */
      "rRxC");

      var HorasPage = /*#__PURE__*/function () {
        function HorasPage(modal, service) {
          _classCallCheck(this, HorasPage);

          this.modal = modal;
          this.service = service;
          this.horasTime = ['10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00', '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00', '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00', '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00'];
          this.hora = [];
        }

        _createClass(HorasPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.getHora();
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modal.dismiss({
              data: '',
              controlHora: false,
              dateFechaReload: this.fecha
            });
          }
        }, {
          key: "select",
          value: function select(data) {
            this.modal.dismiss({
              data: data,
              controlHora: true
            });
          }
        }, {
          key: "getHora",
          value: function getHora() {
            var _this = this;

            this.service.searchReserva(this.fecha).subscribe(function (data) {
              for (var index = 0; index < data.data.length; index++) {
                var hor = data.data[index].hora;

                _this.hora.push(hor);
              }

              for (var _index = 0; _index < _this.hora.length; _index++) {
                _this.horasTime.splice(_this.horasTime.indexOf(_this.hora[_index]), 1);
              }
            });
          }
        }]);

        return HorasPage;
      }();

      HorasPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
        }, {
          type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"]
        }];
      };

      HorasPage.propDecorators = {
        fecha: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"]
        }]
      };
      HorasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-horas',
        template: _raw_loader_horas_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_horas_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
      })], HorasPage);
      /***/
    },

    /***/
    "bECs":
    /*!***************************************!*\
      !*** ./src/app/horas/horas.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function bECs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob3Jhcy5wYWdlLnNjc3MifQ== */";
      /***/
    },

    /***/
    "rRxC":
    /*!*********************************************!*\
      !*** ./src/app/services/service.service.ts ***!
      \*********************************************/

    /*! exports provided: ServiceService */

    /***/
    function rRxC(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ServiceService", function () {
        return ServiceService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "mrSG");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "qCKp");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/operators */
      "kU1M");

      var ServiceService = /*#__PURE__*/function () {
        function ServiceService(http) {
          _classCallCheck(this, ServiceService);

          this.http = http;
          this.path = "https://c2100160.ferozo.com/api/user/";
        }

        _createClass(ServiceService, [{
          key: "getPacientes",
          value: function getPacientes(page, page_size) {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + 'pacientes?page=' + page + '&page_size=' + page_size + '', this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getTratamientos",
          value: function getTratamientos() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "tratamientos", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getReservasDia",
          value: function getReservasDia() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "reservas", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "createReserva",
          value: function createReserva() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
            var fecha = arguments.length > 1 ? arguments[1] : undefined;
            var hora = arguments.length > 2 ? arguments[2] : undefined;
            var tipo = arguments.length > 3 ? arguments[3] : undefined;
            var ficha = arguments.length > 4 ? arguments[4] : undefined;
            var observacion = arguments.length > 5 ? arguments[5] : undefined;
            var tratamiento = arguments.length > 6 ? arguments[6] : undefined;
            var datoaEnviar = {
              "paciente_id": id,
              "fecha": fecha,
              "hora": hora,
              "tipo": tipo,
              // "num_ficha": ficha,
              "observacion": observacion,
              "tratamiento_id": tratamiento
            };
            return this.http.post(this.path + "crearreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "editReserva",
          value: function editReserva(id, hora_ingreso, hora_llegada) {
            var datoaEnviar = {
              "reserva_id": id,
              "hora_ingreso": hora_ingreso,
              "hora_llegada": hora_llegada
            };
            return this.http.post(this.path + "editarreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "deleteReserva",
          value: function deleteReserva(id) {
            var datoaEnviar = {
              "reserva_id": id
            };
            return this.http.post(this.path + "eliminarreserva", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "getEmergenciasDia",
          value: function getEmergenciasDia() {
            this.httpOptions = {
              headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
              })
            };
            return this.http.get(this.path + "emergencias", this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "createEmergencia",
          value: function createEmergencia() {
            var id = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            var fecha = arguments.length > 1 ? arguments[1] : undefined;
            var hora = arguments.length > 2 ? arguments[2] : undefined;
            var tipo = arguments.length > 3 ? arguments[3] : undefined;
            var observacion = arguments.length > 4 ? arguments[4] : undefined;
            var tratamiento = arguments.length > 5 ? arguments[5] : undefined;
            var datoaEnviar = {
              "paciente_id": id,
              "fecha": fecha,
              "hora": hora,
              "tipo": tipo,
              "observacion": observacion,
              "tratamiento_id": tratamiento
            };
            return this.http.post(this.path + "crearemergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "editEmergencia",
          value: function editEmergencia(id, hora_ingreso) {
            var datoaEnviar = {
              "emergencia_id": id,
              "hora_ingreso": hora_ingreso
            };
            return this.http.post(this.path + "editaremergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "deleteEmergencia",
          value: function deleteEmergencia(id) {
            var datoaEnviar = {
              "emergencia_id": id
            };
            return this.http.post(this.path + "eliminaremergencia", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchProductos",
          value: function searchProductos(nombre) {
            var datoaEnviar = {
              "nombre": nombre
            };
            return this.http.post(this.path + "searchpaciente", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchReserva",
          value: function searchReserva(fecha) {
            var datoaEnviar = {
              "fecha": fecha
            };
            return this.http.post(this.path + "reservassearch", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }, {
          key: "searchReservaTratamiento",
          value: function searchReservaTratamiento(fecha) {
            var datoaEnviar = {
              "fecha": fecha
            };
            return this.http.post(this.path + "emergenciassearch", datoaEnviar, this.httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["tap"])(function (data) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(data);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (err) {
              return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(err);
            }));
          }
        }]);

        return ServiceService;
      }();

      ServiceService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      ServiceService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], ServiceService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=horas-horas-module-es5.js.map