(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~create-page-create-page-module~tab1-tab1-module~tab2-tab2-module"],{

/***/ "8MI0":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/horas/horas.page.html ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"end\">\n          <ion-button (click)=\"dismiss()\" color=\"light\">\n            <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\n          </ion-button>\n        </ion-buttons>\n    <ion-title class=\"ion-text-center\">Seleccionar hora</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div class=\"ion-text-center\">\n    <h3>Horas Disponibles</h3>\n    <br>\n    <ion-grid fixed>\n      <ion-row>\n        <ion-col size=\"4\"  *ngFor=\"let item of horasTime\" >\n            <ion-chip  (click)=\"select(item)\" color=\"primary\" mode=\"ios\" outline=\"true\">\n                <ion-icon name=\"time\"></ion-icon>\n                <ion-label>{{item}}</ion-label>\n              </ion-chip>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-card>\n        \n    </ion-card>\n    <br>\n  </div>\n \n\n</ion-content>\n");

/***/ }),

/***/ "AVkp":
/*!***************************************************!*\
  !*** ./src/app/search-page/search-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("#add {\n  color: #27ae60;\n}\n\n.buttons {\n  font-size: 30px;\n  margin-left: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL3NlYXJjaC1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGVBQUE7RUFDQSxpQkFBQTtBQUNKIiwiZmlsZSI6InNlYXJjaC1wYWdlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhZGQge1xuICAgIGNvbG9yOiAjMjdhZTYwO1xufVxuXG4uYnV0dG9ucyB7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xufVxuIl19 */");

/***/ }),

/***/ "LXxN":
/*!*************************************************!*\
  !*** ./src/app/create-page/create-page.page.ts ***!
  \*************************************************/
/*! exports provided: CreatePagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatePagePage", function() { return CreatePagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_create_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./create-page.page.html */ "WaYu");
/* harmony import */ var _create_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./create-page.page.scss */ "iZK/");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../search-page/search-page.page */ "tw2k");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ "wd/R");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../horas/horas.page */ "Y2XA");










let CreatePagePage = class CreatePagePage {
    constructor(formB, service, modalController, loading, toast, modal, alertController) {
        this.formB = formB;
        this.service = service;
        this.modalController = modalController;
        this.loading = loading;
        this.toast = toast;
        this.modal = modal;
        this.alertController = alertController;
        this.hora = [];
        this.horas = '00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23';
        this.horasTime = ['10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00',
            '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00',
            '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00',
            '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00'];
        this.conditionNombreNuevo = false;
        this.conditionHora = false;
    }
    ngOnInit() {
        if (this.tipo == 'reserva') {
            this.form = this.formB.group({
                fecha: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                // hora: ['', Validators.required],
                tipo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            });
        }
        else if (this.tipo == 'emergencia') {
            this.form = this.formB.group({
                fecha: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                hora: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
                tipo: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            });
        }
        this.getTratamientos();
        // this.getHora();
        this.control = false;
        this.actual = moment__WEBPACK_IMPORTED_MODULE_8__().format('YYYY-MM-DD');
        this.actualHora = moment__WEBPACK_IMPORTED_MODULE_8__().format('HH:mm');
    }
    searchCliente() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.conditionNombreNuevo = false;
            const modal = yield this.modalController.create({
                component: _search_page_search_page_page__WEBPACK_IMPORTED_MODULE_7__["SearchPagePage"],
                cssClass: 'my-custom-class',
                swipeToClose: true,
            });
            yield modal.present();
            const { data } = yield modal.onDidDismiss();
            this.control = data.control;
            this.nombres = data.nombres;
            this.apellidos = data.apellidos;
            this.id = data.id;
        });
    }
    searchHora() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.fechaLoad) {
                const modal = yield this.modalController.create({
                    component: _horas_horas_page__WEBPACK_IMPORTED_MODULE_9__["HorasPage"],
                    cssClass: 'my-custom-class',
                    swipeToClose: true,
                    componentProps: {
                        'fecha': this.fechaLoad
                    }
                });
                yield modal.present();
                const { data } = yield modal.onDidDismiss();
                this.controlHora = data.controlHora;
                // this.nombres = data.nombres;
                // this.apellidos = data.apellidos;
                this.dataHour = data.data;
                this.fechaLoad = data.dateFechaReload;
            }
            else {
                this.presentAlertConfirm();
            }
        });
    }
    presentAlertConfirm() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Alerta!',
                message: 'Se equivoco de hora tiene que seleccionar nuevamente la fecha!!!',
                buttons: [
                    {
                        text: 'Ok',
                        handler: () => {
                            this.form.reset();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    eventDate(e) {
        this.fechaLoad = moment__WEBPACK_IMPORTED_MODULE_8__(e.detail.value).format('YYYY-MM-DD');
        this.conditionHora = true;
    }
    getTratamientos() {
        this.service.getTratamientos().subscribe((data) => {
            this.tratamientos = data.data;
        });
    }
    createReserva() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const loading = yield this.loading.create({
                spinner: 'bubbles',
                translucent: true,
            });
            yield loading.present().then(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                if (this.tipo == 'reserva') {
                    if (this.id == '') {
                        this.id = null;
                    }
                    this.service.createReserva(this.id, moment__WEBPACK_IMPORTED_MODULE_8__(this.form.value.fecha).format('YYYY-MM-DD'), this.dataHour, this.form.value.tipo, this.form, this.observacion, this.tratamiento).subscribe((data) => {
                        this.ok('Reserva creada');
                        loading.dismiss();
                        this.dismiss();
                    }, err => {
                        loading.dismiss();
                        this.error('No se registro la reserva');
                    });
                }
                else if (this.tipo == 'emergencia') {
                    this.service.createEmergencia(this.id, moment__WEBPACK_IMPORTED_MODULE_8__(this.form.value.fecha).format('YYYY-MM-DD'), moment__WEBPACK_IMPORTED_MODULE_8__(this.form.value.hora).format('HH:mm'), this.form.value.tipo, this.observacion, this.tratamiento_emergencia).subscribe((data) => {
                        this.ok('Emergencia creada');
                        loading.dismiss();
                        this.dismiss();
                    }, err => {
                        loading.dismiss();
                        this.error('No se registro la emergencia');
                    });
                }
            }));
        });
    }
    dismiss() {
        this.modal.dismiss();
    }
    ok(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-success'
            });
            toast.present();
        });
    }
    error(m) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toast.create({
                message: m,
                duration: 3000,
                cssClass: 'my-custom-class-alert'
            });
            toast.present();
        });
    }
    getHora() {
        this.service.searchReserva(this.dataHour).subscribe((data) => {
            for (let index = 0; index < data.data.length; index++) {
                let hor = data.data[index].hora;
                this.hora.push(hor);
            }
            for (let index = 0; index < this.hora.length; index++) {
                this.horasTime.splice(this.horasTime.indexOf(this.hora[index]), 1);
            }
        });
    }
    openText() {
        this.id = null;
        this.control = false;
        this.conditionNombreNuevo = true;
    }
};
CreatePagePage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"] },
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["AlertController"] }
];
CreatePagePage.propDecorators = {
    tipo: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
CreatePagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-create-page',
        template: _raw_loader_create_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_create_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], CreatePagePage);



/***/ }),

/***/ "WaYu":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/create-page/create-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header *ngIf=\"tipo == 'reserva'\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"dismiss()\" color=\"light\">\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"ion-text-center\">Crear reserva</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"tipo == 'reserva'\">\n  <form [formGroup]=\"form\">\n    <ion-card>\n      <ion-card-content>\n     \n        <ion-item>\n          <ion-label>Fecha</ion-label>\n          <!-- <ion-datetime  (ionChange)=\"eventDate($event)\" formControlName=\"fecha\" interface=\"action-sheet\" min=\"{{actual\n          }}\" max=\"2050\" value=\"{{actual}}\" display-format=\"YYYY-MMM-DD\" placeholder=\"Seleccionar\"\n            monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\">\n          </ion-datetime> -->\n          <ion-input id=\"input-date\"  (ionChange)=\"eventDate($event)\" type=\"date\" formControlName=\"fecha\" min=\"{{actual\n          }}\" max=\"2050\" ></ion-input>\n        </ion-item>\n        <br>\n        <ion-item [disabled]=\"!conditionHora\" (click)=\"searchHora()\">\n          <ion-label >Seleccionar la hora</ion-label>\n          <!-- <ion-datetime minuteValues=\"00,15,30,45\" formControlName=\"hora\" interface=\"action-sheet\" value=\"{{actualHora}}\" min=\"{{actualHora}}\"\n            placeholder=\"Seleccionar\" display-format=\"HH:mm\" picker-format=\"HH:mm\">\n          </ion-datetime> -->\n          <!-- <ion-datetime minuteValues=\"00,15,30,45\"\n            [hourValues]=\"horas\"\n            value=\"{{actualHora}}\" min=\"{{actualHora}}\"\n            formControlName=\"hora\" interface=\"action-sheet\" placeholder=\"Seleccionar\" display-format=\"HH:mm\"\n            picker-format=\"HH:mm\">\n          </ion-datetime> -->\n          <!-- <ion-select formControlName=\"hora\"  ok-text=\"Seleccionar\" cancel-text=\"Cancelar\">\n            <ion-select-option *ngFor=\"let item of horasTime\" [value]=\"item\">{{item}}</ion-select-option>\n          </ion-select> -->\n        <h6 *ngIf=\"controlHora == true\">{{dataHour}}</h6>\n        <h6 *ngIf=\"controlHora == false\">Seleccionar la hora</h6>\n\n        </ion-item>\n        <br>\n        <div  class=\"ion-text-center\">\n          <!-- <ion-item> -->\n            <ion-button (click)=\"searchCliente()\" expand=\"block\">\n              <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\n              <h6 *ngIf=\"control == true\">{{apellidos + ' ' + nombres}}</h6>\n              <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\n            </ion-button>\n            <br>\n            <ion-button (click)=\"openText()\" expand=\"block\">\n                <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>Nuevo cliente\n              </ion-button>\n          <!-- </ion-item> -->\n          <ion-item *ngIf=\"conditionNombreNuevo\" >\n              <ion-label>Nombre de Cliente</ion-label>\n             <ion-input [(ngModel)]=\"observacion\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" ></ion-input>\n            </ion-item>\n        </div>\n        <br>\n        <ion-item>\n          <ion-label>Tipo</ion-label>\n          <ion-select formControlName=\"tipo\" interface=\"action-sheet\" cancelText=\"Cancelar\" placeholder=\"Seleccionar\">\n            <ion-select-option value=\"Consulta\">Consulta</ion-select-option>\n            <ion-select-option value=\"Reconsulta\">Reconsulta</ion-select-option>\n            <ion-select-option value=\"Tratamiento\">Tratamiento</ion-select-option>\n          </ion-select>\n        </ion-item>\n        <!-- <ion-item>\n          <ion-label>Ficha</ion-label>\n          <ion-input type=\"number\" placeholder=\"Número de ficha\" formControlName=\"numFicha\">\n          </ion-input>\n        </ion-item> -->\n        <br>\n        <ion-item>\n          <ion-label>Tratamiento</ion-label>\n          <ion-select [(ngModel)]=\"tratamiento\" [ngModelOptions]=\"{standalone: true}\" interface=\"action-sheet\" cancelText=\"Cancelar\"\n            placeholder=\"Seleccionar\">\n            <ion-select-option *ngFor=\"let item of tratamientos\" value=\"{{item.id}}\">{{item.nombre | titlecase}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n        <div class=\"ion-text-center margin\">\n          <ion-button [disabled]=\"!form.valid  || controlHora == false\" (click)=\"createReserva()\" type=\"submit\" shape=\"round\"\n            id=\"principal-color-button\">\n            Registrar\n          </ion-button>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </form>\n</ion-content>\n\n<ion-header *ngIf=\"tipo == 'emergencia'\">\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"dismiss()\" color=\"light\">\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title class=\"ion-text-center\">Crear emergencia</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"tipo == 'emergencia'\">\n  <form [formGroup]=\"form\">\n    <ion-card>\n      <ion-card-content>\n        <!-- <ion-item > -->\n          <div class=\"ion-text-center\">\n              <ion-button (click)=\"searchCliente()\" expand=\"block\">\n                  <h6 *ngIf=\"control == false\">Seleccionar el paciente</h6>\n                  <h6 *ngIf=\"control == true\">{{apellidos + ' ' + nombres}}</h6>\n                  <ion-icon *ngIf=\"control == false\" name=\"add-circle\" slot=\"start\"></ion-icon>\n                </ion-button>\n                <br>\n                <ion-button (click)=\"openText()\" expand=\"block\">\n                    <ion-icon name=\"add-circle\" slot=\"start\"></ion-icon>Nuevo cliente\n                  </ion-button>\n                <ion-item *ngIf=\"conditionNombreNuevo\" >\n                  <ion-label>Nombre de Cliente</ion-label>\n                 <ion-input [(ngModel)]=\"observacion\" [ngModelOptions]=\"{standalone: true}\" type=\"text\" ></ion-input>\n                </ion-item>\n          </div>\n          \n        <!-- </ion-item> -->\n        <br>\n        <ion-item>\n          <ion-label>Seleccionar la hora</ion-label>\n          <ion-datetime minuteValues=\"00,15,30,45\" formControlName=\"hora\" interface=\"action-sheet\"\n            placeholder=\"Seleccionar\" display-format=\"HH:mm\"\n            picker-format=\"HH:mm\">\n          </ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-label>Fecha</ion-label>\n          <!-- <ion-datetime formControlName=\"fecha\" interface=\"action-sheet\" min=\"{{actual\n          }}\" max=\"2050\" value=\"{{actual}}\" display-format=\"YYYY-MMM-DD\" placeholder=\"Seleccionar\"\n            monthShortNames=\"Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre\">\n          </ion-datetime> -->\n          <ion-input id=\"input-date\" min=\"{{actual\n          }}\" max=\"2050\" type=\"date\" formControlName=\"fecha\" ></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label>Tipo</ion-label>\n          <ion-select formControlName=\"tipo\" interface=\"action-sheet\" cancelText=\"Cancelar\" placeholder=\"Seleccionar\">\n            <ion-select-option value=\"Consulta\">Consulta</ion-select-option>\n            <ion-select-option value=\"Reconsulta\">Reconsulta</ion-select-option>\n            <ion-select-option value=\"Tratamiento\">Tratamiento</ion-select-option>\n          </ion-select>\n        </ion-item>\n        <ion-item>\n          <ion-label>Tratamiento</ion-label>\n          <ion-select [(ngModel)]=\"tratamiento_emergencia\" [ngModelOptions]=\"{standalone: true}\" interface=\"action-sheet\" cancelText=\"Cancelar\"\n            placeholder=\"Seleccionar\">\n            <ion-select-option *ngFor=\"let item of tratamientos\" value=\"{{item.id}}\">{{item.nombre | titlecase}}\n            </ion-select-option>\n          </ion-select>\n        </ion-item>\n        <div class=\"ion-text-center margin\">\n          <ion-button [disabled]=\" !form.valid \" (click)=\"createReserva()\" type=\"submit\" shape=\"round\"\n            id=\"principal-color-button\">\n            Registrar\n          </ion-button>\n        </div>\n      </ion-card-content>\n    </ion-card>\n  </form>\n</ion-content>");

/***/ }),

/***/ "Y2XA":
/*!*************************************!*\
  !*** ./src/app/horas/horas.page.ts ***!
  \*************************************/
/*! exports provided: HorasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HorasPage", function() { return HorasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_horas_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./horas.page.html */ "8MI0");
/* harmony import */ var _horas_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./horas.page.scss */ "bECs");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");






let HorasPage = class HorasPage {
    constructor(modal, service) {
        this.modal = modal;
        this.service = service;
        this.horasTime = ['10:00:00', '10:15:00', '10:30:00', '10:45:00', '11:00:00', '11:15:00', '11:30:00', '11:45:00', '12:00:00', '12:15:00',
            '12:30:00', '12:45:00', '13:00:00', '13:15:00', '13:30:00', '13:45:00', '14:00:00', '14:15:00', '14:30:00', '14:45:00', '15:00:00',
            '15:15:00', '15:30:00', '15:45:00', '16:00:00', '16:15:00', '16:30:00', '16:45:00', '17:00:00', '17:15:00', '17:30:00', '17:45:00',
            '18:00:00', '18:15:00', '18:30:00', '18:45:00', '19:00:00', '19:15:00', '19:30:00', '19:45:00', '20:00:00'];
        this.hora = [];
    }
    ngOnInit() {
        this.getHora();
    }
    dismiss() {
        this.modal.dismiss({
            data: '',
            controlHora: false,
            dateFechaReload: this.fecha
        });
    }
    select(data) {
        this.modal.dismiss({
            data: data,
            controlHora: true
        });
    }
    getHora() {
        this.service.searchReserva(this.fecha).subscribe((data) => {
            for (let index = 0; index < data.data.length; index++) {
                let hor = data.data[index].hora;
                this.hora.push(hor);
            }
            for (let index = 0; index < this.hora.length; index++) {
                this.horasTime.splice(this.horasTime.indexOf(this.hora[index]), 1);
            }
        });
    }
};
HorasPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] }
];
HorasPage.propDecorators = {
    fecha: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Input"] }]
};
HorasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-horas',
        template: _raw_loader_horas_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_horas_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HorasPage);



/***/ }),

/***/ "bECs":
/*!***************************************!*\
  !*** ./src/app/horas/horas.page.scss ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob3Jhcy5wYWdlLnNjc3MifQ== */");

/***/ }),

/***/ "iZK/":
/*!***************************************************!*\
  !*** ./src/app/create-page/create-page.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".margin {\n  margin-top: 100px;\n}\n\nion-input[type=date]:before {\n  content: attr(placeholder) !important;\n  color: #aaa;\n  margin-right: 0.5em;\n}\n\nion-input[type=date]:focus:before,\nion-input[type=date]:valid:before {\n  content: \"\";\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2NyZWF0ZS1wYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0FBQ0o7O0FBV0E7RUFDSSxxQ0FBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQVJKOztBQVVFOztFQUVFLFdBQUE7QUFQSiIsImZpbGUiOiJjcmVhdGUtcGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFyZ2luIHtcbiAgICBtYXJnaW4tdG9wOiAxMDBweDtcbn1cbi8vIGEsIGEgZGl2LCBhIHNwYW4sIGEgaW9uLWljb24sIGEgaW9uLWxhYmVsLCBidXR0b24sIGJ1dHRvbiBkaXYsIGJ1dHRvbiBzcGFuLCBidXR0b24gaW9uLWljb24sIGJ1dHRvbiBpb24tbGFiZWwsIC5pb24tdGFwcGFibGUsIFt0YXBwYWJsZV0sIFt0YXBwYWJsZV0gZGl2LCBbdGFwcGFibGVdIHNwYW4sIFt0YXBwYWJsZV0gaW9uLWljb24sIFt0YXBwYWJsZV0gaW9uLWxhYmVsLCBpbnB1dCwgdGV4dGFyZWF7XG4vLyAgICAgY29sb3I6IHdoaXRlO1xuLy8gICAgIGJhY2tncm91bmQ6ICMzODgwZmY7XG4vLyAgICAgYm9yZGVyOiBub25lO1xuLy8gfVxuLy8gI2lucHV0LWRhdGV7XG4vLyAgICAgLS1jb2xvcjogd2hpdGU7XG4vLyAgICAgLS1iYWNrZ3JvdW5kOiAjMzg4MGZmO1xuLy8gICAgIC8vIGJvcmRlcjogbm9uZTtcbi8vIH1cbmlvbi1pbnB1dFt0eXBlPVwiZGF0ZVwiXTpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IGF0dHIocGxhY2Vob2xkZXIpICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6ICNhYWE7XG4gICAgbWFyZ2luLXJpZ2h0OiAwLjVlbTtcbiAgfVxuICBpb24taW5wdXRbdHlwZT1cImRhdGVcIl06Zm9jdXM6YmVmb3JlLFxuICBpb24taW5wdXRbdHlwZT1cImRhdGVcIl06dmFsaWQ6YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICB9Il19 */");

/***/ }),

/***/ "kUx7":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search-page/search-page.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"end\">\n      <ion-button (click)=\"dismiss()\" color=\"light\">\n        <ion-icon id=\"icon-color\" style=\"font-size: 35px;\" name=\"close-circle-outline\"></ion-icon>\n      </ion-button>\n    </ion-buttons>\n    <ion-title>Buscar cliente</ion-title>\n \n  </ion-toolbar>\n</ion-header>\n<ion-searchbar placeholder=\"Buscar...\" [(ngModel)]=\"filterTerm\" animated=\"true\" [debounce]=\"2000\"\n(ionChange)=\"searchProductos()\"\n[autocomplete]=\"true\"\n\n></ion-searchbar>\n<ion-content>\n\n\n  <ion-list>\n    <ion-item *ngFor=\"let data of itemListData \" (click)=\"select(data)\">\n      <ion-label>{{(data.apellidos | titlecase) + ' ' + (data.nombres | titlecase)}}</ion-label>\n      <ion-icon id=\"add\" (click)=\"select(data)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\n    </ion-item>\n    <!-- <ion-virtual-scroll [items]=\"pacientes\" approxItemHeight=\"320px\">\n        <ion-card *virtualItem=\"let item;\">\n            <ion-item (click)=\"select(item)\">\n                <ion-label>{{(item.apellidos | titlecase) + ' ' + (item.nombres | titlecase)}}</ion-label>\n                <ion-icon id=\"add\" (click)=\"select(item)\" name=\"add-circle\" class=\"buttons\"></ion-icon>\n              </ion-item>\n        </ion-card>\n      </ion-virtual-scroll> -->\n    \n  </ion-list>\n  <ion-infinite-scroll (ionInfinite)=\"doInfinite($event)\">\n      <ion-infinite-scroll-content loadingSpinner=\"bubbles\" loadingText=\"Loading more data...\">\n      </ion-infinite-scroll-content>\n    </ion-infinite-scroll>\n</ion-content>");

/***/ }),

/***/ "tw2k":
/*!*************************************************!*\
  !*** ./src/app/search-page/search-page.page.ts ***!
  \*************************************************/
/*! exports provided: SearchPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchPagePage", function() { return SearchPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./search-page.page.html */ "kUx7");
/* harmony import */ var _search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-page.page.scss */ "AVkp");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _services_service_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/service.service */ "rRxC");






let SearchPagePage = class SearchPagePage {
    constructor(service, loading, modal) {
        this.service = service;
        this.loading = loading;
        this.modal = modal;
        this.itemListData = [];
        this.page_number = 1;
        this.page_limit = 100;
    }
    ngOnInit() {
        this.getPacientes(false, "");
    }
    getPacientes(isFirstLoad, event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            // const loading = await this.loading.create({
            //   spinner: 'bubbles',
            //   translucent: true,
            // });
            // await loading.present().then(async () => {
            this.service.getPacientes(this.page_number, this.page_limit).subscribe((data) => {
                data = data.data;
                for (let i = 0; i < data.length; i++) {
                    this.itemListData.push(data[i]);
                }
                if (isFirstLoad)
                    event.target.complete();
                this.page_number++;
                // loading.present();
            }, error => {
                //  loading.present();
            });
            // })
        });
    }
    doInfinite(event) {
        this.getPacientes(true, event);
    }
    select(data) {
        this.modal.dismiss({
            id: data.id,
            nombres: data.nombres,
            apellidos: data.apellidos,
            control: true
        });
    }
    dismiss() {
        this.modal.dismiss({
            id: '',
            nombres: '',
            apellidos: '',
            control: false
        });
    }
    searchProductos() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.filterTerm === '') {
                this.page_number = 1;
                this.getPacientes(false, "");
            }
            else {
                this.itemListData = [];
                const loading = yield this.loading.create({
                    spinner: 'bubbles',
                    translucent: true,
                });
                yield loading.present().then(() => {
                    this.service.searchProductos(this.filterTerm).subscribe((data) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                        this.itemListData = data.data;
                        yield loading.dismiss();
                    }), err => {
                        loading.dismiss();
                    });
                });
            }
        });
    }
};
SearchPagePage.ctorParameters = () => [
    { type: _services_service_service__WEBPACK_IMPORTED_MODULE_5__["ServiceService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
SearchPagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-search-page',
        template: _raw_loader_search_page_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_search_page_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], SearchPagePage);



/***/ })

}]);
//# sourceMappingURL=default~create-page-create-page-module~tab1-tab1-module~tab2-tab2-module-es2015.js.map